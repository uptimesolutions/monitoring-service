/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicetelemetry.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.EchoDiagnosticsDAO;
import com.uptime.cassandra.monitor.dao.MistDiagnosticsDAO;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadDeviceTelemetryDelegate {
    private final MistDiagnosticsDAO mistDiagnosticsDAO;
    private final EchoDiagnosticsDAO echoDiagnosticsDAO;

    /**
     * Constructor
     */
    public ReadDeviceTelemetryDelegate() {
        mistDiagnosticsDAO = BaseStationMapperImpl.getInstance().mistDiagnosticsDAO();
        echoDiagnosticsDAO = BaseStationMapperImpl.getInstance().echoDiagnosticsDAO();
    }

    /**
     * Return a List of MistDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<MistDiagnostics> getMistDiagnosticsByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return mistDiagnosticsDAO.findByCustomerSite(customer, siteId);
    }

    /**
     * Return a List of MistDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<MistDiagnostics> getMistDiagnosticsByCustomerSiteDevice(String customer, UUID siteId, String echoDeviceId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return mistDiagnosticsDAO.findByCustomerSiteDevice(customer, siteId, echoDeviceId, deviceId);
    }

    /**
     * Return a List of MistDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param echoDeviceId the serial # of the echobase associated to this Mist
     * @param deviceId, the serial # of the Mist
     * @param channelNum, byte
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<MistDiagnostics> getMistDiagnosticsByPK(String customer, UUID siteId, String echoDeviceId, String deviceId, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return mistDiagnosticsDAO.findByPK(customer, siteId, echoDeviceId, deviceId, channelNum);
    }

    /**
     * Return a List of EchoDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<EchoDiagnostics> getEchoDiagnosticsByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return echoDiagnosticsDAO.findByCustomerSite(customer, siteId);
    }

    /**
     * Return a List of EchoDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationHostname, String Object
     * @param deviceId, String Object
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<EchoDiagnostics> getEchoDiagnosticsByPK(String customer, UUID siteId, String baseStationHostname, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return echoDiagnosticsDAO.findByPK(customer, siteId, baseStationHostname, deviceId);
    }
}

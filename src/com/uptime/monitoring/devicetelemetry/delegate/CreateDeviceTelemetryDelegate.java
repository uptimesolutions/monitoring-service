/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicetelemetry.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.EchoDiagnosticsDAO;
import com.uptime.cassandra.monitor.dao.MistDiagnosticsDAO;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class CreateDeviceTelemetryDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDeviceTelemetryDelegate.class.getName());
    private final MistDiagnosticsDAO mistDiagnosticsDAO;
    private final EchoDiagnosticsDAO echoDiagnosticsDAO;

    /**
     * Constructor
     */
    public CreateDeviceTelemetryDelegate() {
        mistDiagnosticsDAO = BaseStationMapperImpl.getInstance().mistDiagnosticsDAO();
        echoDiagnosticsDAO = BaseStationMapperImpl.getInstance().echoDiagnosticsDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param deviceTelemetryVO, DeviceTelemetryVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createMistDiagnostics(DeviceTelemetryVO deviceTelemetryVO) throws IllegalArgumentException {
        String errorMsg;
        MistDiagnostics mistDiagnostics;
        
        // Insert the entities into Cassandra.
        try {
            mistDiagnostics = DelegateUtil.getMistDiagnostics(deviceTelemetryVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(mistDiagnostics)) == null) {
                mistDiagnosticsDAO.create(mistDiagnostics);
                return "{\"outcome\":\"New MistDiagnostics created / updated successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Error: Failed to create / update MistDiagnostics.\"}";
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param deviceTelemetryVO, DeviceTelemetryVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createEchoDiagnostics(DeviceTelemetryVO deviceTelemetryVO) throws IllegalArgumentException {
        String errorMsg;
        EchoDiagnostics echoDiagnostics;
        
        // Insert the entities into Cassandra.
        try {
            echoDiagnostics = DelegateUtil.getEchoDiagnostics(deviceTelemetryVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(echoDiagnostics)) == null) {
                try {
                    echoDiagnosticsDAO.create(echoDiagnostics);
                    return "{\"outcome\":\"New EchoDiagnostics created / updated successfully.\"}";
                } catch (UnavailableException | WriteTimeoutException e) {
                    sendEvent(e.getStackTrace());
                }
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Error: Failed to create / update EchoDiagnostics.\"}";
    }

}

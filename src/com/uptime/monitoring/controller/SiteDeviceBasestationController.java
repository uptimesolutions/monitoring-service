/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.monitoring.devicebasestation.delegate.ReadSiteDeviceBasestationDelegate;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class SiteDeviceBasestationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SiteDeviceBasestationController.class.getName());
    ReadSiteDeviceBasestationDelegate readDelegate;

    public SiteDeviceBasestationController() {
        readDelegate = new ReadSiteDeviceBasestationDelegate();
    }

    /**
     * Return a json of a list of SiteDeviceBasestation Objects by the given
     * params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteDeviceBasestationByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteDeviceBasestation> siteDeviceBasestationResult;
        StringBuilder json;

        if ((siteDeviceBasestationResult = readDelegate.getSiteDeviceBasestationByCustomerSite(customer, UUID.fromString(siteId))) != null && !siteDeviceBasestationResult.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer_acct\":\"").append(customer).append("\",")
                    .append("\"site_id\":\"").append(siteId).append("\",")
                    .append("\"points\":[");
            for (int i = 0; i < siteDeviceBasestationResult.size(); i++) {
                json
                        .append("{")
                        .append("\"device_id\":\"").append(siteDeviceBasestationResult.get(i).getDeviceId()).append("\",")
                        .append("\"base_station_port\":").append(siteDeviceBasestationResult.get(i).getBaseStationPort()).append(",")
                        .append("\"is_disabled\":").append(siteDeviceBasestationResult.get(i).isDisabled()).append("")
                        .append("}").append(i < siteDeviceBasestationResult.size() - 1 ? "," : "");
            }
            json.append("]}");
            return json.toString();
        } else {
            return "{\"outcome\":\"No items found for the given info.\"}";
        }
    }

    public static void main(String arg[]) {
        SiteDeviceBasestationController c = new SiteDeviceBasestationController();
        try {
            c.getSiteDeviceBasestationByCustomerSite("FEDEX EXPRESS", "8b086e15-371f-4a8d-bedb-c1b67b49be3e");
        } catch (Exception exp) {
            System.out.println("--EXp" + exp.getMessage());

        }
    }
}

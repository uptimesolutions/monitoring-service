/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import com.uptime.monitoring.delegate.CreateMonitorDelegate;
import com.uptime.monitoring.delegate.DeleteMonitorDelegate;
import com.uptime.monitoring.delegate.ReadMonitorDelegate;
import com.uptime.monitoring.delegate.UpdateMonitorDelegate;
import com.uptime.monitoring.utils.JsonUtil;
import com.uptime.monitoring.vo.MonitorVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class MonitorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorController.class.getName());
    ReadMonitorDelegate readDelegate;

    public MonitorController() {
        readDelegate = new ReadMonitorDelegate();
    }

    /**
     * Return a json of a list of ApprovedBaseStations objects for the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getMonitorByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApprovedBaseStations> result;
        StringBuilder json;

        if ((result = readDelegate.getMonitorByCustomerSiteId(customer, UUID.fromString(siteId))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"approvedBaseStations\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"No items found for the given info.\"}";
        }
    }

    /**
     * Return a json of a list of ApprovedBaseStations Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param macAddress, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getMonitorByPK(String customer, String siteId, String macAddress) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApprovedBaseStations> result;
        StringBuilder json;

        if ((result = readDelegate.getMonitorByPK(customer, UUID.fromString(siteId), macAddress)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customerAccount\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"macAddress\":\"").append(macAddress).append("\",")
                    .append("\"approvedBaseStations\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"No items found for the given info.\"}";
        }
    }

    /**
     * Create a new ApprovedBaseStations by inserting into approved_base_stations table
     *
     * @param content, String Object
     * @return String Object
     */
    public String createApprovedBaseStations(String content) {
        MonitorVO monitorVO;
        CreateMonitorDelegate createMonitorDelegate;

        if ((monitorVO = JsonUtil.parser(content)) != null) {
            if (monitorVO.getCustomerAccount() != null && !monitorVO.getCustomerAccount().isEmpty()
                    && monitorVO.getSiteId() != null
                    && monitorVO.getMacAddress() != null && !monitorVO.getMacAddress().isEmpty()) {

                // Insert into Cassandra
                try {
                    createMonitorDelegate = new CreateMonitorDelegate();
                    return createMonitorDelegate.createApprovedBaseStations(monitorVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new ApprovedBaseStations.\"}";
                }

            } else {
                LOGGER.warn("Insufficient and/or invalid data given in json");
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            LOGGER.warn("Json is invalid");
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given ApprovedBaseStations
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateApprovedBaseStations(String content) {
        MonitorVO monitorVO;
        UpdateMonitorDelegate updateMonitorDelegate;

        if ((monitorVO = JsonUtil.parser(content)) != null) {
            if (monitorVO.getCustomerAccount() != null && !monitorVO.getCustomerAccount().isEmpty()
                    && monitorVO.getSiteId() != null
                    && monitorVO.getMacAddress() != null && !monitorVO.getMacAddress().isEmpty()) {

                // Update in Cassandra
                try {
                    updateMonitorDelegate = new UpdateMonitorDelegate();
                    return updateMonitorDelegate.updateApprovedBaseStations(monitorVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update ApprovedBaseStations items.\"}";
                }
            } else {
                LOGGER.warn("Insufficient and/or invalid data given in json");
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            LOGGER.warn("Json is invalid");
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given ApprovedBaseStations
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteApprovedBaseStations(String content) {
        MonitorVO monitorVO;
        DeleteMonitorDelegate deleteMonitorDelegate;

        if ((monitorVO = JsonUtil.parser(content)) != null) {
            if (monitorVO.getCustomerAccount() != null && !monitorVO.getCustomerAccount().isEmpty()
                    && monitorVO.getSiteId() != null
                    && monitorVO.getMacAddress() != null && !monitorVO.getMacAddress().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteMonitorDelegate = new DeleteMonitorDelegate();
                    return deleteMonitorDelegate.deleteApprovedBaseStations(monitorVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete ApprovedBaseStations items.\"}";
                }

            } else {
                LOGGER.warn("Insufficient and/or invalid data given in json");
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            LOGGER.warn("Json is invalid");
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}

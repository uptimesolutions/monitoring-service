/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import com.uptime.monitoring.gatewaylastupload.delegate.ReadGatewayLastUploadDelegate;
import com.uptime.monitoring.gatewaylastupload.delegate.UpdateGatewayLastUploadDelegate;
import com.uptime.monitoring.utils.JsonUtil;
import com.uptime.monitoring.vo.GatewayLastUploadVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class GatewayLastUploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayLastUploadController.class.getName());
    private final ReadGatewayLastUploadDelegate readDelegate;

    /**
     * Constructor
     */
    public GatewayLastUploadController() {
        readDelegate = new ReadGatewayLastUploadDelegate();
    }

    /**
     * Return a List of GatewayLastUpload Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getGatewayLastUploadByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GatewayLastUpload> result;
        StringBuilder json;

        if ((result = readDelegate.getGatewayLastUploadByCustomerSiteId(customer.trim(), UUID.fromString(siteId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"gatewayLastUploads\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of GatewayLastUpload Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param deviceId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getGatewayLastUploadByPK(String customer, String siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<GatewayLastUpload> result;
        StringBuilder json;

        if ((result = readDelegate.getGatewayLastUploadByPK(customer.trim(), UUID.fromString(siteId.trim()), deviceId.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customerAccount\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"deviceId\":\"").append(deviceId).append("\",")
                        .append("\"gatewayLastUploads\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Update tables dealing with the given ApprovedBaseStations
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateGatewayLastUpload(String content) {
        List<GatewayLastUploadVO> gatewayLastUploadVOList;
        UpdateGatewayLastUploadDelegate updateGatewayLastUploadDelegate;

        if ((gatewayLastUploadVOList = JsonUtil.gatewayLastUploadVOParser(content)) != null && !gatewayLastUploadVOList.isEmpty()) {
            for (GatewayLastUploadVO gatewayLastUploadVO : gatewayLastUploadVOList) {
                if (gatewayLastUploadVO == null || gatewayLastUploadVO.getSiteId() == null || gatewayLastUploadVO.getLastUpload() == null ||
                        gatewayLastUploadVO.getCustomerAccount() == null || gatewayLastUploadVO.getCustomerAccount().isEmpty() ||
                        gatewayLastUploadVO.getDeviceId() == null || gatewayLastUploadVO.getDeviceId().isEmpty() ||
                        gatewayLastUploadVO.getSiteName() == null || gatewayLastUploadVO.getSiteName().isEmpty()) {
                    LOGGER.warn("Insufficient and/or invalid data given in json");
                    return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
                }
            }

            // Update in Cassandra
            updateGatewayLastUploadDelegate = new UpdateGatewayLastUploadDelegate();
            return updateGatewayLastUploadDelegate.updateGatewayLastUpload(gatewayLastUploadVOList);
            
        } else {
            LOGGER.warn("Json is invalid");
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import com.uptime.monitoring.devicetelemetry.delegate.CreateDeviceTelemetryDelegate;
import com.uptime.monitoring.devicetelemetry.delegate.ReadDeviceTelemetryDelegate;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import com.uptime.services.util.JsonConverterUtil;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeviceTelemetryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceTelemetryController.class.getName());

    ReadDeviceTelemetryDelegate readDelegate;

    public DeviceTelemetryController() {
        readDelegate = new ReadDeviceTelemetryDelegate();
    }

    /**
     * Return a List of EchoDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getEchoDiagnosticsByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<EchoDiagnostics> result;
        StringBuilder json;

        if ((result = readDelegate.getEchoDiagnosticsByCustomerSiteId(customer.trim(), UUID.fromString(siteId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customer\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"EchoDiagnostics\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of EchoDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getMostRecentEchoDiagnosticsByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<EchoDiagnostics> result;
        StringBuilder json;

        if ((result = readDelegate.getEchoDiagnosticsByCustomerSiteId(customer.trim(), UUID.fromString(siteId.trim()))) != null) {
            result = findMostRecentEchoDiagnosticsByDeviceId(result);
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customer\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"EchoDiagnostics\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of MistDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getMistDiagnosticsByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<MistDiagnostics> result;
        StringBuilder json;

        if ((result = readDelegate.getMistDiagnosticsByCustomerSiteId(customer.trim(), UUID.fromString(siteId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customer\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"MistDiagnostics\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of MistDiagnostics Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getMostRecentMistDiagnosticsByCustomerSiteId(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<MistDiagnostics> result;
        StringBuilder json;

        if ((result = readDelegate.getMistDiagnosticsByCustomerSiteId(customer.trim(), UUID.fromString(siteId.trim()))) != null) {
            result = findMostRecentMistDiagnosticsByDeviceId(result);
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customer\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId).append("\",")
                        .append("\"MistDiagnostics\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new EchoDiagnostic by inserting into echo_diagnostic table or a new EchoDiagnostic by inserting into echo_diagnostic table
     *
     * @param deviceTelemetryVO
     * @return String Object
     */
    public String createEchoDiagnostic(DeviceTelemetryVO deviceTelemetryVO) {
        CreateDeviceTelemetryDelegate createDeviceTelemetryDelegate;

        if (deviceTelemetryVO != null) {
            // Insert into Cassandra
            try {
                createDeviceTelemetryDelegate = new CreateDeviceTelemetryDelegate();
                return createDeviceTelemetryDelegate.createEchoDiagnostics(deviceTelemetryVO);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new EchoDiagnostic.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Create a new MistDiagnostics by inserting into mist_diagnostics table or a new MistDiagnostics by inserting into mist_diagnostics table
     *
     * @param deviceTelemetryVO
     * @return String Object
     */
    public String createMistDiagnostic(DeviceTelemetryVO deviceTelemetryVO) {
        CreateDeviceTelemetryDelegate createDeviceTelemetryDelegate;

        if (deviceTelemetryVO != null) {

            // Insert into Cassandra
            try {
                createDeviceTelemetryDelegate = new CreateDeviceTelemetryDelegate();
                return createDeviceTelemetryDelegate.createMistDiagnostics(deviceTelemetryVO);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new MistDiagnostics.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Finds the most recent EchoDiagnostics objects for each device ID in the given list.
     *
     * @param list A list of EchoDiagnostics objects.
     * @return A list of EchoDiagnostics objects, each representing the most recent data available for its respective device ID.
     */
    public List<EchoDiagnostics> findMostRecentEchoDiagnosticsByDeviceId(List<EchoDiagnostics> list) {
        Map<String, EchoDiagnostics> mostRecentByDeviceId = new HashMap<>();
        List<EchoDiagnostics> echoDiagnosticsList = new ArrayList();
        for (EchoDiagnostics obj : list) {
            String deviceId = obj.getDeviceId();
            if (!mostRecentByDeviceId.containsKey(deviceId)) {
                mostRecentByDeviceId.put(deviceId, obj);
            } else {
                if (obj.getLastUpdate().isAfter(mostRecentByDeviceId.get(deviceId).getLastUpdate())) {
                    mostRecentByDeviceId.put(deviceId, obj);
                }
            }
        }
        for (EchoDiagnostics obj : mostRecentByDeviceId.values()) {
            echoDiagnosticsList.add(obj);
        }
        return echoDiagnosticsList;
    }

    /**
     * Finds the most recent MistDiagnostics objects for each device ID in the given list.
     *
     * @param list A list of MistDiagnostics objects.
     * @return A list of MistDiagnostics objects, each representing the most recent data available for its respective device ID.
     */
    public List<MistDiagnostics> findMostRecentMistDiagnosticsByDeviceId(List<MistDiagnostics> list) {
        Map<String, MistDiagnostics> mostRecentByDeviceId = new HashMap<>();
        List<MistDiagnostics> mistDiagnosticsList = new ArrayList();
        for (MistDiagnostics obj : list) {
            String deviceId = obj.getDeviceId();
            if (!mostRecentByDeviceId.containsKey(deviceId)) {
                mostRecentByDeviceId.put(deviceId, obj);
            } else {
                if (obj.getLastSample().isAfter(mostRecentByDeviceId.get(deviceId).getLastSample())) {
                    mostRecentByDeviceId.put(deviceId, obj);
                }
            }
        }
        for (MistDiagnostics obj : mostRecentByDeviceId.values()) {
            mistDiagnosticsList.add(obj);
        }
        return mistDiagnosticsList;
    }

    public static void main(String args[]) {
        try {
            DeviceTelemetryController dtc = new DeviceTelemetryController();
            DeviceTelemetryVO deviceTelemetryVO = new DeviceTelemetryVO();
            deviceTelemetryVO.setCustomerAccount("FEDEX EXPRESS");
            deviceTelemetryVO.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
            deviceTelemetryVO.setBaseStationHostname("Test BaseStationHost1");
            deviceTelemetryVO.setDeviceId("BB001235");
            deviceTelemetryVO.setEchoDeviceId("BB001235");
            deviceTelemetryVO.setEchoFw("Test EchoFW1");
            deviceTelemetryVO.setEchoBacklog((short) 1);
            deviceTelemetryVO.setEchoPowerfault((short) 2);
            deviceTelemetryVO.setEchoReboot((short) 3);
            deviceTelemetryVO.setLastUpdate(Instant.now());
            String msg = JsonConverterUtil.toJSON(deviceTelemetryVO);
            dtc.createEchoDiagnostic(deviceTelemetryVO);
            System.out.println(msg);
        } catch (Exception exp) {
            System.out.println("exp--> " + exp.getMessage());
        }

    }

}

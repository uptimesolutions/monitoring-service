/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.monitoring.devicebasestation.delegate.CreateSiteBasestationDeviceDelegate;
import com.uptime.monitoring.devicebasestation.delegate.ReadSiteBasestationDeviceDelegate;
import com.uptime.monitoring.devicebasestation.delegate.UpdateSiteBasestationDeviceDelegate;
import com.uptime.monitoring.utils.JsonUtil;
import com.uptime.monitoring.vo.SiteDeviceBasestationVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class SiteBasestationDeviceController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteBasestationDeviceController.class.getName());
    ReadSiteBasestationDeviceDelegate readDelegate;

    public SiteBasestationDeviceController() {
        readDelegate = new ReadSiteBasestationDeviceDelegate();
    }

    /**
     * Return a json of a list of ApprovedBaseStations Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param baseStationPort, short
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteBasestationDevice(String customer, String siteId, short baseStationPort) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteBasestationDevice> siteBasestationDeviceResult;
        StringBuilder json;

        if ((siteBasestationDeviceResult = readDelegate.getSiteBasestationDeviceByCustomerSiteBasestation(customer, UUID.fromString(siteId), baseStationPort)) != null && !siteBasestationDeviceResult.isEmpty()) {
            json = new StringBuilder();
            json
                    .append("{\"customer_acct\":\"").append(customer).append("\",")
                    .append("\"site_id\":\"").append(siteId).append("\",")
                    .append("\"base_station_port\":\"").append(baseStationPort).append("\",")
                    .append("\"points\":[");
            for (int i = 0; i < siteBasestationDeviceResult.size(); i++) {
                json
                        .append("{")
                        .append("\"device_id\":\"").append(siteBasestationDeviceResult.get(i).getDeviceId()).append("\",")
                        .append("\"point_id\":\"").append(siteBasestationDeviceResult.get(i).getPointId()).append("\",")
                        .append("\"channel_type\":\"").append(siteBasestationDeviceResult.get(i).getChannelType()).append("\",")
                        .append("\"sensor_channel_num\":").append(siteBasestationDeviceResult.get(i).getSensorChannelNum()).append(",")
                        .append("\"sample_interval\":").append(siteBasestationDeviceResult.get(i).getSampleInterval()).append(",")
                        .append("\"fmax\":").append(siteBasestationDeviceResult.get(i).getFmax()).append(",")
                        .append("\"resolution\":").append(siteBasestationDeviceResult.get(i).getResolution()).append(",")
                        .append("\"is_disabled\":").append(siteBasestationDeviceResult.get(i).isDisabled()).append(",")
                        .append("\"last_sampled\":\"").append(siteBasestationDeviceResult.get(i).getLastSampled()).append("\"")
                        .append("}").append(i < siteBasestationDeviceResult.size() - 1 ? "," : "");
            }
            json.append("]}");
            return json.toString();
        } else {
            return "{\"outcome\":\"No items found for the given info.\"}";
        }
    }

    /**
     * Insert into tables dealing with the given SiteBasestationDevice
     *
     * @param content, String Object
     * @return String Object
     */
    public String createSiteBasestationDevice(String content) {
        CreateSiteBasestationDeviceDelegate createSiteBasestationDeviceDelegate;
        SiteDeviceBasestationVO siteDeviceBasestationVO;

        if ((siteDeviceBasestationVO = JsonUtil.siteDeviceBasestationVOParser(content)) != null) {
            if (siteDeviceBasestationVO.getCustomerAccount() != null && !siteDeviceBasestationVO.getCustomerAccount().isEmpty()
                    && siteDeviceBasestationVO.getSiteId() != null
                    && siteDeviceBasestationVO.getDeviceId() != null && !siteDeviceBasestationVO.getDeviceId().isEmpty()
                    && siteDeviceBasestationVO.getBaseStationPort() != 0) {

                // Update in Cassandra
                try {
                    createSiteBasestationDeviceDelegate = new CreateSiteBasestationDeviceDelegate();
                    return createSiteBasestationDeviceDelegate.createSiteDeviceBasestation(siteDeviceBasestationVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create SiteBasestationDevice items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given SiteBasestationDevice
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateSiteBasestationDevice(String content) {
        UpdateSiteBasestationDeviceDelegate updateSiteBasestationDeviceDelegate;
        SiteDeviceBasestationVO siteDeviceBasestationVO;

        LOGGER.info( "CONTENT:{0}", content);
        if ((siteDeviceBasestationVO = JsonUtil.siteDeviceBasestationVOParser(content)) != null) {
            if (siteDeviceBasestationVO.getCustomerAccount() != null && !siteDeviceBasestationVO.getCustomerAccount().isEmpty()
                    && siteDeviceBasestationVO.getSiteId() != null
                    && siteDeviceBasestationVO.getDeviceId() != null && !siteDeviceBasestationVO.getDeviceId().isEmpty()
                    && siteDeviceBasestationVO.getBaseStationPort() != 0
                    && siteDeviceBasestationVO.getPointId() != null) {

                // Update the last_sampled for the given device
                try {
                    updateSiteBasestationDeviceDelegate = new UpdateSiteBasestationDeviceDelegate();
                    return updateSiteBasestationDeviceDelegate.updateSiteBasestationDeviceLastSampled(siteDeviceBasestationVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update SiteBasestationDevice items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring;

import static com.uptime.services.ServiceConstants.DEVELOPING_TESTING;
import com.uptime.services.vo.ServiceHostVO;
import com.uptime.services.vo.EventVO;
import com.uptime.monitoring.http.listeners.RequestListener;
import com.uptime.services.AbstractServiceNew;
import java.net.InetAddress;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class MonitorService extends AbstractServiceNew {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorService.class.getName());
    public final static String SERVICE_NAME = "Monitoring";
    public static String IP_ADDRESS = null;
    public static int PORT = 0;
    public static Semaphore mutex = new Semaphore(1);
    public static boolean running = true;
    private static RequestListener listener;
    public static String[] names = {"Events"};
    private static final AtomicLong EVENT_INDEX = new AtomicLong(0L);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            // get the network port number from the command line parameter
            if (args.length != 1) {
                System.out.println("Usage: java -jar MonitorService.jar <port>");
                System.exit(1);
            }
            
            // check port number id valid
            try {
                if ((PORT = Integer.parseInt(args[0])) < 1024) {
                    System.out.println("Port must be > 1024.");
                    System.exit(1);
                }
            } catch (NumberFormatException e) {
                System.out.println("Port must be an integer.");
                System.exit(1);
            }

            // get IP address of local host
            IP_ADDRESS = InetAddress.getLocalHost().getHostAddress();

            System.out.println("Service Port: " + PORT);
            System.out.println("Service Ip Address: " + IP_ADDRESS);

            startup();
            manageService();
            shutdown();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.exit(1);
        }
    }

    /**
     * Manage the service thread.
     */
    private static void manageService() {
        //Query Check
        while (running) {
            try {
                //Update required services
                if (running) {
                    for (int i = 0; i < 300; i++) {
                        if (!running) {
                            break;
                        }
                        Thread.sleep(1000);
                    }

                    if (names != null) {
                        for (String name : names) {
                            try {
                                mutex.acquire();
                                if (!query(name)) {
                                    System.out.println("Query failed for " + name);
                                    LOGGER.info("Query failed for {}", name);
                                }
                            } finally {
                                mutex.release();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                sendEvent(e.getStackTrace());
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Startup the service, start requestsListener, and registering the service
     *
     * @throws Exception
     */
    private static void startup() throws Exception {
        System.out.println("Startup initiated...");
        LOGGER.info("Startup initiated...");
        boolean success;

        // Starting Listener
        listener = new RequestListener(PORT);
        listener.start();

        try {
            mutex.acquire();

            // Registering
            System.out.println("Registering service...");
            LOGGER.info("Registering service...");
            do {
                if (!(success = register(SERVICE_NAME, IP_ADDRESS, PORT))) {
                    Thread.sleep(2000L);
                    System.out.println("Retry Registering service...");
                }
            } while (success == false);
            System.out.println("Service instance registered successfully.");
            LOGGER.info("Service instance registered successfully.");

            // Subscribing
            if (names != null) {
                System.out.println("Subscribing...");
                LOGGER.info("Subscribing...");
                do {
                    if (!(success = subscribe(names, IP_ADDRESS, PORT, "", "service"))) {
                        Thread.sleep(2000L);
                        System.out.println("Retry Subscribing...");
                    }
                } while (success == false);
                System.out.println("Subscribing successfully.");
                LOGGER.info("Subscribing successfully.");
            }
        } catch (Exception e) {
            LOGGER.error("Startup failed.");
            LOGGER.error(e.getMessage(), e);
        } finally {
            mutex.release();
        }
    }

    /**
     * Shuts down the service cleanly by unregistering the service, stopping all threads, and closing the Cassandra cluster connection.
     */
    private static void shutdown() {
        System.out.println("Shut down initiated...");
        LOGGER.info("Shut down initiated...");
        boolean success;

        try {
            // Unsubscribing
            if (names != null) {
                System.out.println("Unsubscribing...");
                LOGGER.info("Unsubscribing...");
                do {
                    if (!(success = unsubscribe(names, IP_ADDRESS, PORT, "", "service"))) {
                        Thread.sleep(2000L);
                        System.out.println("Retry Unsubscribing...");
                    }
                } while (success == false);
                System.out.println("Unsubscribing successfully.");
                LOGGER.info("Unsubscribing successfully.");
            }

            // Unregistering
            System.out.println("Unregistering service instance...");
            LOGGER.info("Unregistering service instance...");
            do {
                if (!(success = unregister(SERVICE_NAME, IP_ADDRESS, PORT))) {
                    Thread.sleep(2000L);
                    System.out.println("Retry Unregistering service...");
                }
            } while (success == false);
            System.out.println("Service instance unregistered successfully.");
            LOGGER.info("Service instance unregistered successfully.");
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
            LOGGER.error(e.getMessage(), e);
        }

        listener.stop();
        LOGGER.info("Exiting.");
        System.exit(0);
    }

    /**
     * Attempt to send an event to the Events service
     *
     * @param stackTrace, Array of StackTraceElement objects
     */
    public static void sendEvent(StackTraceElement[] stackTrace) {
        if (!DEVELOPING_TESTING) {
            StringBuilder data = new StringBuilder();
            EventVO evo;
            ServiceHostVO current;
            String service = "Events";
            boolean emailing = true;
            int attempts, count = 0;
            Long createdDate = new Date().getTime();

            try {
                for (StackTraceElement ele : stackTrace) {
                    data.append(ele.toString()).append("<br />");
                }

                // set EventVO
                evo = new EventVO();
                evo.setData(data.toString());
                evo.setApplication(SERVICE_NAME);
                evo.setIpAddress(IP_ADDRESS);
                evo.setPort(String.valueOf(PORT));
                evo.setCreatedDate(createdDate);

                while (emailing && count < 2) {

                    // Query Events service if needed
                    try {
                        mutex.acquire();
                        if (!getServiceHosts().containsKey(service) || getServiceHosts().get(service).isEmpty()) {
                            query(service);
                        }
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    } finally {
                        mutex.release();
                    }

                    // Attempt to send event
                    if (!getServiceHosts().get(service).isEmpty()) {
                        attempts = 0;
                        do {
                            try {
                                current = getServiceHosts().get(service).get((int) EVENT_INDEX.get());
                                try {
                                    sendEvent(evo, current.getIp(), current.getPort()); // Sending Email
                                    attempts = 5;
                                    emailing = false;
                                } catch (Exception e) {
                                    LOGGER.warn(e.getMessage(), e);

                                    // Publishing CircuitBreaker
                                    try {
                                        mutex.acquire();
                                        publishCircuitBreaker(current, service);
                                    } catch (Exception ex) {
                                        LOGGER.error(ex.getMessage(), ex);
                                    } finally {
                                        mutex.release();
                                    }
                                    ++attempts;
                                }
                                EVENT_INDEX.incrementAndGet();
                            } catch (IndexOutOfBoundsException e) {
                                if (EVENT_INDEX.get() == 0L) {
                                    break;
                                } else {
                                    EVENT_INDEX.set(0L);
                                }
                            }
                        } while (attempts < 5);
                    }
                    ++count;
                }
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }

    @Override
    public void sendEmail(String content) {
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicebasestation.delegate;

import com.uptime.cassandra.dao.UpdateStatementsDAO;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.SiteDeviceBasestationVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateSiteBasestationDeviceDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteBasestationDeviceDelegate.class.getName());
    private final UpdateStatementsDAO updateDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;

    /**
     * Constructor
     */
    public UpdateSiteBasestationDeviceDelegate() {
        updateDAO = new UpdateStatementsDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
    }

    /**
     * Updates site_basestation_device.last_sampled
     *
     * @param siteDeviceBasestationVO, SiteDeviceBasestationVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateSiteBasestationDevice(SiteDeviceBasestationVO siteDeviceBasestationVO) throws IllegalArgumentException {
        SiteBasestationDevice siteBasestationDevice;
        String errorMsg;
        
        if (siteDeviceBasestationVO != null) {
            // Insert updated entities into Cassandra

            try {
                siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(siteDeviceBasestationVO);
                
                if ((errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null) {
                    siteBasestationDeviceDAO.create(siteBasestationDevice);
                    return "{\"outcome\":\"Updated SiteBasestationDevice successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to update SiteBasestationDevice.\"}";
    }

    public String updateSiteBasestationDeviceLastSampled(SiteDeviceBasestationVO siteDeviceBasestationVO) throws IllegalArgumentException {
        SiteBasestationDevice sbd;

        try {
            sbd = new SiteBasestationDevice();
            sbd.setCustomerAccount(siteDeviceBasestationVO.getCustomerAccount());
            sbd.setSiteId(siteDeviceBasestationVO.getSiteId());
            sbd.setBaseStationPort(siteDeviceBasestationVO.getBaseStationPort());
            sbd.setDeviceId(siteDeviceBasestationVO.getDeviceId());
            sbd.setPointId(siteDeviceBasestationVO.getPointId());
            sbd.setLastSampled(siteDeviceBasestationVO.getLastSampled());

            updateDAO.updateSiteBasestationDeviceLastSampled(sbd);
            return "{\"outcome\":\"Updated SiteBasestationDevice successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update SiteBasestationDevice.\"}";
    }

}

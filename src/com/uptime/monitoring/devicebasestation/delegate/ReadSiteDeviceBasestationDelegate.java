/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicebasestation.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadSiteDeviceBasestationDelegate {
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    
    public ReadSiteDeviceBasestationDelegate(){
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }
    
    /**
     * Return a List of SiteDeviceBasestation Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     *
     * @return List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDeviceBasestation> getSiteDeviceBasestationByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDeviceBasestationDAO.findByCustomerSite(customer, siteId);
    }
    
     /**
     * Return a List of SiteDeviceBasestation Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId. String Object
     *
     * @return List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDeviceBasestation> getSiteDeviceBasestationByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDeviceBasestationDAO.findByPK(customer, siteId, deviceId);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicebasestation.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadSiteBasestationDeviceDelegate {
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    
    /**
     * Constructor
     */
    public ReadSiteBasestationDeviceDelegate() {
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
    }

    /**
     * Return a List of SiteBasestationDevice Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     *
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBasestationDevice> getSiteBasestationDeviceByCustomerSiteBasestation(String customer, UUID siteId, short baseStationPort) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBasestationDeviceDAO.findByCustomerSiteBasestation(customer, siteId, baseStationPort);
    }

    /**
     * Return a List of SiteBasestationDevice Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId. String Object
     *
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBasestationDevice> getSiteBasestationDeviceByCustomerSiteBasestationDevice(String customer, UUID siteId, short baseStationPort, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBasestationDeviceDAO.findByCustomerSiteBasestationDevice(customer, siteId, baseStationPort, deviceId);
    }

    /**
     * Return a List of SiteBasestationDevice Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId. String Object
     * @param pointId, UUID Object
     *
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBasestationDevice> getSiteBasestationDeviceByPK(String customer, UUID siteId, short baseStationPort, String deviceId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBasestationDeviceDAO.findByPK(customer, siteId, baseStationPort, deviceId, pointId);
    }

}

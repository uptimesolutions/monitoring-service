/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.devicebasestation.delegate;

import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.SiteDeviceBasestationVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateSiteBasestationDeviceDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteBasestationDeviceDelegate.class.getName());
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;

    /**
     * Constructor
     */
    public CreateSiteBasestationDeviceDelegate() {
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }

    /**
     * Insert Rows in Cassandra based on the given object
     *
     * @param siteDeviceBasestationVO, SiteDeviceBasestationVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createSiteDeviceBasestation(SiteDeviceBasestationVO siteDeviceBasestationVO) throws IllegalArgumentException {
        SiteDeviceBasestation newSiteDeviceBasestation;
        SiteBasestationDevice newSiteBasestationDevice;
        String errorMsg;

        newSiteBasestationDevice = DelegateUtil.getSiteBasestationDevice(siteDeviceBasestationVO);
        newSiteDeviceBasestation = DelegateUtil.getSiteDeviceBasestation(siteDeviceBasestationVO);

        // Insert entities into Cassandra 
        try {
            if (((errorMsg = ServiceUtil.validateObjectData(newSiteBasestationDevice)) == null)
                    && ((errorMsg = ServiceUtil.validateObjectData(newSiteDeviceBasestation)) == null)) {
                siteBasestationDeviceDAO.create(newSiteBasestationDevice);
                siteDeviceBasestationDAO.create(newSiteDeviceBasestation);
                return "{\"outcome\":\"Created SiteDeviceBasestation successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create SiteDeviceBasestation.\"}";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.gatewaylastupload.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.GatewayLastUploadDAO;
import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ReadGatewayLastUploadDelegate {
    private final GatewayLastUploadDAO gatewayLastUploadDAO;

    /**
     * Constructor
     */
    public ReadGatewayLastUploadDelegate() {
        gatewayLastUploadDAO = BaseStationMapperImpl.getInstance().gatewayLastUploadDAO();
    }

    /**
     * Return a List of GatewayLastUpload Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of GatewayLastUpload Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GatewayLastUpload> getGatewayLastUploadByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return gatewayLastUploadDAO.findByCustomerSiteId(customer, siteId);
    }

    /**
     * Return a List of GatewayLastUpload Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     * @return List of GatewayLastUpload Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GatewayLastUpload> getGatewayLastUploadByPK(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return gatewayLastUploadDAO.findByPK(customer, siteId, deviceId);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.gatewaylastupload.delegate;

import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.GatewayLastUploadDAO;
import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.GatewayLastUploadVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class UpdateGatewayLastUploadDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateGatewayLastUploadDelegate.class.getName());
    private final GatewayLastUploadDAO gatewayLastUploadDAO;

    /**
     * Constructor
     */
    public UpdateGatewayLastUploadDelegate() {
        gatewayLastUploadDAO = BaseStationMapperImpl.getInstance().gatewayLastUploadDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param gatewayLastUploadVOList, List Object of GatewayLastUploadVO Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateGatewayLastUpload(List<GatewayLastUploadVO> gatewayLastUploadVOList) {
        boolean oneOrMoreSuccess = false;
        
        for (GatewayLastUpload gatewayLastUpload : DelegateUtil.getGatewayLastUploadList(gatewayLastUploadVOList)) {

            // Insert updated entities into Cassandra 
            try {
                if (ServiceUtil.validateObjectData(gatewayLastUpload) == null) {
                    gatewayLastUploadDAO.update(gatewayLastUpload);
                    oneOrMoreSuccess = true;
                }
            } catch (IllegalArgumentException e) {
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }

        }
        
        return oneOrMoreSuccess ? "{\"outcome\":\"ApprovedBaseStations updated successfully.\"}" : "{\"outcome\":\"Error: Failed to update GatewayLastUpload items.\"}";
    }
}

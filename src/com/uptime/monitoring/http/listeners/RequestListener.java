/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.http.listeners;

import com.sun.net.httpserver.HttpServer;
import com.uptime.monitoring.http.handlers.BaseStationHandler;
import com.uptime.monitoring.http.handlers.DeviceTelemetryHandler;
import com.uptime.monitoring.http.handlers.GatewayLastUploadHandler;
import com.uptime.monitoring.http.handlers.ServiceHelperHandler;
import com.uptime.monitoring.http.handlers.SystemHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class RequestListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestListener.class.getName());
    private int port = 0;
    HttpServer server;
    
    /**
     * Parameterized Constructor
     * @param port, int 
     */
    public RequestListener(int port) {
        this.port = port;
    }
    
    /**
     * Stop the Http server
     */
    public void stop() {
        LOGGER.info("Stopping request listener...");
        server.stop(10);
    }
    
    /**
     * Start the Http Server with the needed handlers
     * @throws Exception 
     */
    public void start() throws Exception {
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/basestation", new BaseStationHandler());
        server.createContext("/servicehelper", new ServiceHelperHandler());
        server.createContext("/device-telemetry", new DeviceTelemetryHandler());
        server.createContext("/gateway-last-upload", new GatewayLastUploadHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.info("Starting request listener...");
        server.start();
    }
}

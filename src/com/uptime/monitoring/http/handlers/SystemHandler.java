/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.http.handlers;

import static com.uptime.monitoring.MonitorService.IP_ADDRESS;
import static com.uptime.monitoring.MonitorService.PORT;
import static com.uptime.monitoring.MonitorService.SERVICE_NAME;
import static com.uptime.monitoring.MonitorService.mutex;
import static com.uptime.monitoring.MonitorService.names;
import static com.uptime.monitoring.MonitorService.running;
import com.uptime.services.http.handler.AbstractSystemHandlerVersion2;
import java.util.concurrent.Semaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author gsingh
 */
public class SystemHandler extends AbstractSystemHandlerVersion2 {
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemHandler.class.getName());
    /**
     * Returns the field 'running'
     * @return boolean
     */
    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Sets the field 'running'
     * @param value 
     */
    @Override
    public void setRunning(boolean value) {
        running = value;
    }

    /**
     * Returns the field 'LOGGER'
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Returns the field 'SERVICE_NAME'
     * @return String Object
     */
    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    /**
     * Returns the field 'PORT'
     * @return int
     */
    @Override
    public int getServicePort() {
        return PORT;
    }

    /**
     * Returns the field 'IP_ADDRESS'
     * @return String
     */
    @Override
    public String getServiceIpAddress() {
        return IP_ADDRESS;
    }

    /**
     * Returns the field 'names'
     * @return Array of String Objects
     */
    @Override
    public String[] getServiceSubscribeNames() {
        return names;
    }

    /**
     * Returns the field 'mutex'
     * @return Semaphore Object
     */
    @Override
    public Semaphore getMutex() {
        return mutex;
    }

    /**
     * Used to send an event to the EventsService
     * @param stackTrace Array of StackTraceElement Objects
     */
    @Override
    public void sendEvent(StackTraceElement[] stackTrace) {
        com.uptime.monitoring.MonitorService.sendEvent(stackTrace);
    }
}

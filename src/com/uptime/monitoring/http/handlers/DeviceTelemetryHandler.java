/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.http.handlers;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.monitoring.MonitorService.running;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.controller.DeviceTelemetryController;
import com.uptime.monitoring.utils.JsonUtil;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import com.uptime.services.http.handler.AbstractGenericHandlerNew;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeviceTelemetryHandler extends AbstractGenericHandlerNew {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceTelemetryHandler.class.getName());
    DeviceTelemetryController controller;

    public DeviceTelemetryHandler() {
        controller = new DeviceTelemetryController();
    }

    /**
     * HTTP GET handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        String resp;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        Object object;

        try {
            if ((params = parseQuery(he.getRequestURI().getRawQuery())).isEmpty()) {
                resp = "{\"outcome\":\"Hello World!\"}";
            } else {
                LOGGER.info("Query: {}", new Object[]{he.getRequestURI().getQuery()});
                for (String key : params.keySet()) {
                    if ((object = params.get(key)) != null) {
                        LOGGER.info("{}: {}", new Object[]{key, object.toString()});
                    } else {
                        LOGGER.info("{}: null", new Object[]{key});
                    }
                }
                if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("echoDiagnostics") && params.containsKey("mostRecentByDeviceId")) {
                    try {
                        resp = controller.getMostRecentEchoDiagnosticsByCustomerSiteId(params.get("customer").toString(), params.get("siteId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and areaId are required.\"}";
                    }
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("echoDiagnostics")) {
                    try {
                        resp = controller.getEchoDiagnosticsByCustomerSiteId(params.get("customer").toString(), params.get("siteId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and areaId are required.\"}";
                    }
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("mistDiagnostics") && params.containsKey("mostRecentByDeviceId")) {
                    try {
                        resp = controller.getMostRecentMistDiagnosticsByCustomerSiteId(params.get("customer").toString(), params.get("siteId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and areaId are required.\"}";
                    }
                } else if (params.containsKey("customer") && params.containsKey("siteId") && params.containsKey("mistDiagnostics")) {
                    try {
                        resp = controller.getMistDiagnosticsByCustomerSiteId(params.get("customer").toString(), params.get("siteId").toString());
                    } catch (NumberFormatException | NullPointerException e) {
                        resp = "{\"outcome\":\"A valid customer, siteId and areaId are required.\"}";
                    }
                } else {
                    resp = "{\"outcome\":\"Unknown Params\"}";
                }
            }

            response = resp.getBytes();
            if (resp.contains("Unknown Params") || (resp.contains("A valid") && resp.contains("required."))) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else if (resp.contains("No items found")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (ReadTimeoutException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"ReadTimeoutException\"}".getBytes()), he);
        } catch (UnavailableException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnavailableException\"}".getBytes()), he);
        } catch (UnsupportedEncodingException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes()), he);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }

        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }

    /**
     * HTTP POST handler
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        String content;
        DeviceTelemetryVO vo;
        OutputStream os;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        String resp = null;
        byte[] response = null;

        try {
            if (he.getRequestBody() != null) {
                content = streamReader(he.getRequestBody());
                LOGGER.info("*********JSON VALUE:{}", content);

                // convert the JSON to a DeviceTelemetryVO
                vo = JsonUtil.dTParser(content);

                // content contains data for both echo diagnostic and mist diagnostic so upsert a row for each
                controller.createEchoDiagnostic(vo);
                controller.createMistDiagnostic(vo);
                resp = "{\"outcome\":\"Success\"}";
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
            else{
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
                response = resp.getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
        }
        
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
    }

    /**
     * Returns the field 'running'
     *
     * @return boolean
     */
    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     *
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Set the HttpExchange response header and return a StackTraceElement Array Object
     *
     * @param exception, Exception Object
     * @param response, byte Array Object
     * @param httpExchange, HttpExchange Object
     * @return StackTraceElement Array Object
     * @throws IOException
     */
    private StackTraceElement[] setStackTrace(final Exception exception, byte[] response, HttpExchange httpExchange) throws IOException {
        LOGGER.error(exception.getMessage(), exception);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
        return exception.getStackTrace();
    }
}

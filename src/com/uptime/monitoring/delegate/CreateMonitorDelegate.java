/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.delegate;

import com.uptime.cassandra.monitor.dao.ApprovedBaseStationsDAO;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.MonitorVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class CreateMonitorDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateMonitorDelegate.class.getName());
    private final ApprovedBaseStationsDAO approvedBaseStationsDAO;

    /**
     * Constructor
     */
    public CreateMonitorDelegate() {
        approvedBaseStationsDAO = BaseStationMapperImpl.getInstance().approvedBaseStationsDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param monitoringVO, MonitorVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createApprovedBaseStations(MonitorVO monitoringVO) throws IllegalArgumentException {
        String errorMsg;
        ApprovedBaseStations approvedBaseStations;

        // Insert the entities into Cassandra.
        try {
            approvedBaseStations = DelegateUtil.getApprovedBaseStations(monitoringVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(approvedBaseStations)) == null) {
                approvedBaseStationsDAO.create(approvedBaseStations);
                return "{\"outcome\":\"New ApprovedBaseStations created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new ApprovedBaseStations.\"}";
    }
}

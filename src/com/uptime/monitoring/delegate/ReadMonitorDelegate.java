/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.ApprovedBaseStationsDAO;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class ReadMonitorDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadMonitorDelegate.class.getName());
    private final ApprovedBaseStationsDAO approvedBaseStationsDAO;

    /**
     * Constructor
     */
    public ReadMonitorDelegate() {
        approvedBaseStationsDAO = BaseStationMapperImpl.getInstance().approvedBaseStationsDAO();
    }

    /**
     * Return a List of ApprovedBaseStations Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApprovedBaseStations> getMonitorByCustomerSiteId(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return approvedBaseStationsDAO.findByCustomerSiteId(customer, siteId);
    }

    /**
     * Return a List of ApprovedBaseStations Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param macAddress, String Object
     * @return List of ApprovedBaseStations Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApprovedBaseStations> getMonitorByPK(String customer, UUID siteId, String macAddress) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return approvedBaseStationsDAO.findByPK(customer, siteId, macAddress);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.monitor.dao.ApprovedBaseStationsDAO;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.vo.MonitorVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class DeleteMonitorDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteMonitorDelegate.class.getName());
    private final ApprovedBaseStationsDAO approvedBaseStationsDAO;

    /**
     * Constructor
     */
    public DeleteMonitorDelegate() {
        approvedBaseStationsDAO = BaseStationMapperImpl.getInstance().approvedBaseStationsDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param monitoringVO, MonitorVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteApprovedBaseStations(MonitorVO monitoringVO) throws IllegalArgumentException {
        List<ApprovedBaseStations> approvedBaseStationsList;
        ApprovedBaseStations approvedBaseStations = null;

        // Find the entities based on the given values
        try {
            if ((approvedBaseStationsList = approvedBaseStationsDAO.findByPK(monitoringVO.getCustomerAccount(), monitoringVO.getSiteId(), monitoringVO.getMacAddress())) != null && !approvedBaseStationsList.isEmpty()) {
                approvedBaseStations = approvedBaseStationsList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (approvedBaseStations != null) {
                approvedBaseStationsDAO.delete(approvedBaseStations);
                return "{\"outcome\":\"ApprovedBaseStations deleted successfully.\"}";
            }
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"No ApprovedBaseStations found to delete.\"}";
    }
}

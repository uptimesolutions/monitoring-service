/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.delegate;

import com.uptime.cassandra.monitor.dao.ApprovedBaseStationsDAO;
import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import static com.uptime.monitoring.MonitorService.sendEvent;
import com.uptime.monitoring.utils.DelegateUtil;
import com.uptime.monitoring.vo.MonitorVO;
import com.uptime.services.util.ServiceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class UpdateMonitorDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateMonitorDelegate.class.getName());
    private final ApprovedBaseStationsDAO approvedBaseStationsDAO;

    /**
     * Constructor
     */
    public UpdateMonitorDelegate() {
        approvedBaseStationsDAO = BaseStationMapperImpl.getInstance().approvedBaseStationsDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param monitoringVO
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateApprovedBaseStations(MonitorVO monitoringVO) throws IllegalArgumentException {
        ApprovedBaseStations approvedBaseStations;
        String errorMsg;

        // Insert updated entities into Cassandra 
        try {
            approvedBaseStations = DelegateUtil.getApprovedBaseStations(monitoringVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(approvedBaseStations)) == null) {
                approvedBaseStationsDAO.create(approvedBaseStations);
                return "{\"outcome\":\"ApprovedBaseStations updated successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update ApprovedBaseStations.\"}";
    }

}

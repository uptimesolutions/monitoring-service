/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.vo;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class GatewayLastUploadVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String deviceId;
    private String siteName;
    private Instant lastUpload;

    public GatewayLastUploadVO() {
    }

    public GatewayLastUploadVO(GatewayLastUploadVO gatewayLastUploadVO) {
        customerAccount = gatewayLastUploadVO.getCustomerAccount();
        siteId = gatewayLastUploadVO.getSiteId();
        deviceId = gatewayLastUploadVO.getDeviceId();
        siteName = gatewayLastUploadVO.getSiteName();
        lastUpload = gatewayLastUploadVO.getLastUpload();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Instant getLastUpload() {
        return lastUpload;
    }

    public void setLastUpload(Instant lastUpload) {
        this.lastUpload = lastUpload;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.deviceId);
        hash = 79 * hash + Objects.hashCode(this.siteName);
        hash = 79 * hash + Objects.hashCode(this.lastUpload);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GatewayLastUploadVO other = (GatewayLastUploadVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastUpload, other.lastUpload)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GatewayLastUploadVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceId=" + deviceId + ", siteName=" + siteName + ", lastUpload=" + lastUpload + '}';
    }
}

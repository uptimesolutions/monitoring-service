/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.vo;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class SiteDeviceBasestationVO {
    
    private String customerAccount;
    private UUID siteId;
    private short baseStationPort;
    private String deviceId;
    private UUID pointId;
    private String channelType;
    private int fmax;
    private Instant lastSampled;
    private int resolution;
    private short sampleInterval ;
    private byte sensorChannelNum;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public Instant getLastSampled() {
        return lastSampled;
    }

    public void setLastSampled(Instant lastSampled) {
        this.lastSampled = lastSampled;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public byte getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(byte sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.customerAccount);
        hash = 53 * hash + Objects.hashCode(this.siteId);
        hash = 53 * hash + this.baseStationPort;
        hash = 53 * hash + Objects.hashCode(this.deviceId);
        hash = 53 * hash + Objects.hashCode(this.pointId);
        hash = 53 * hash + Objects.hashCode(this.channelType);
        hash = 53 * hash + this.fmax;
        hash = 53 * hash + Objects.hashCode(this.lastSampled);
        hash = 53 * hash + this.resolution;
        hash = 53 * hash + this.sampleInterval;
        hash = 53 * hash + this.sensorChannelNum;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteDeviceBasestationVO other = (SiteDeviceBasestationVO) obj;
        if (this.baseStationPort != other.baseStationPort) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.lastSampled, other.lastSampled)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteDeviceBasestationVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", baseStationPort=" + baseStationPort + ", deviceId=" + deviceId + ", pointId=" + pointId + ", channelType=" + channelType + ", fmax=" + fmax + ", lastSampled=" + lastSampled + ", resolution=" + resolution + ", sampleInterval=" + sampleInterval + ", sensorChannelNum=" + sensorChannelNum + "}";
    }
}

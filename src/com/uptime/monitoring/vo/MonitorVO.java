/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.vo;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class MonitorVO {
    private String customerAccount;
    private UUID siteId;
    private String macAddress;
    private UUID issuedToken;
    private Instant lastPasscode;
    private UUID passcode;

    public MonitorVO() {
    }

    public MonitorVO(MonitorVO monitoringVO) {
        customerAccount = monitoringVO.getCustomerAccount();
        siteId = monitoringVO.getSiteId();
        macAddress = monitoringVO.getMacAddress();
        issuedToken = monitoringVO.getIssuedToken();
        lastPasscode = monitoringVO.getLastPasscode();
        passcode = monitoringVO.getPasscode();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public UUID getIssuedToken() {
        return issuedToken;
    }

    public void setIssuedToken(UUID issuedToken) {
        this.issuedToken = issuedToken;
    }

    public Instant getLastPasscode() {
        return lastPasscode;
    }

    public void setLastPasscode(Instant lastPasscode) {
        this.lastPasscode = lastPasscode;
    }

    public UUID getPasscode() {
        return passcode;
    }

    public void setPasscode(UUID passcode) {
        this.passcode = passcode;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.customerAccount);
        hash = 31 * hash + Objects.hashCode(this.siteId);
        hash = 31 * hash + Objects.hashCode(this.macAddress);
        hash = 31 * hash + Objects.hashCode(this.issuedToken);
        hash = 31 * hash + Objects.hashCode(this.lastPasscode);
        hash = 31 * hash + Objects.hashCode(this.passcode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MonitorVO other = (MonitorVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.macAddress, other.macAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.issuedToken, other.issuedToken)) {
            return false;
        }
        if (!Objects.equals(this.lastPasscode, other.lastPasscode)) {
            return false;
        }
        if (!Objects.equals(this.passcode, other.passcode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MonitorVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", macAddress=" + macAddress + ", issuedToken=" + issuedToken + ", lastPasscode=" + lastPasscode + ", passcode=" + passcode + '}';
    }
}

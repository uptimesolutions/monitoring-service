/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.vo;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class DeviceTelemetryVO {
    private String customerAccount;
    private UUID siteId;
    private String baseStationHostname;
    private String deviceId;
    private String echoDeviceId;
    private String mistDeviceId;
    private String deviceType;
    private String echoFw;
    private short echoDatasets;
    private short echoBacklog;
    private byte echoRadioFail;
    private byte echoTerr;
    private short echoRestart;
    private byte echoPowLevel;
    private short echoReboot;
    private short echoPowerfault;
    private Instant lastUpdate; //use echobase_time from json
    private byte mistlxChan; //hex convert decimal
    private Instant lastSample;
    private byte mistlxRssi;
    private byte mistlxFsamp; //hex convert decimal
    private short mistlxLength;
    private float mistlxBattery; //divide by 51.2
    private float mistlxTemp; // (((value * .4185) * 9) / 5) +32
    private short mistlxDelta;
    private short mistlxPcorr;
    private byte mistlxDbg; //hex convert decima
    private byte mistlxBleon; //hex convert decimal
    private byte mistlxFpgaon; //hex convert decimal
    private short mistlxTotalMin; //hex convert decimal
    private short mistlxMincnt;
    private short mistlxFailcnt; //hex convert decimal
    private short mistlxCtime; //hex convert decimal
    private short mistlxCtimeMod;
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private String pointName;
    private short baseStationPort;
    private String baseStationMac;
    
    public DeviceTelemetryVO(){
        
    }
    
    public DeviceTelemetryVO(DeviceTelemetryVO deviceTelemetryVO){
        customerAccount = deviceTelemetryVO.getCustomerAccount();
        siteId = deviceTelemetryVO.getSiteId();
        baseStationHostname = deviceTelemetryVO.getBaseStationHostname();
        deviceId = deviceTelemetryVO.getDeviceId();
        deviceType = deviceTelemetryVO.getDeviceType();
        echoFw = deviceTelemetryVO.getEchoFw();
        echoDatasets = deviceTelemetryVO.getEchoDatasets();
        echoBacklog = deviceTelemetryVO.getEchoBacklog();
        echoRadioFail = deviceTelemetryVO.getEchoRadioFail();
        echoTerr = deviceTelemetryVO.getEchoTerr();
        echoRestart = deviceTelemetryVO.getEchoRestart();
        echoPowLevel = deviceTelemetryVO.getEchoPowLevel();
        echoReboot = deviceTelemetryVO.getEchoReboot();
        echoPowerfault = deviceTelemetryVO.getEchoPowerfault();
        lastUpdate = deviceTelemetryVO.getLastUpdate(); 
        mistlxChan = deviceTelemetryVO.getMistlxChan();
        lastSample = deviceTelemetryVO.getLastSample();
        mistlxRssi = deviceTelemetryVO.getMistlxRssi();
        mistlxFsamp = deviceTelemetryVO.getMistlxFsamp();
        mistlxLength = deviceTelemetryVO.getMistlxLength();
        mistlxBattery = deviceTelemetryVO.getMistlxBattery();
        mistlxTemp = deviceTelemetryVO.getMistlxTemp();
        mistlxDelta = deviceTelemetryVO.getMistlxDelta();
        mistlxPcorr = deviceTelemetryVO.getMistlxPcorr();
        mistlxDbg = deviceTelemetryVO.getMistlxDbg();
        mistlxBleon = deviceTelemetryVO.getMistlxBleon(); 
        mistlxFpgaon = deviceTelemetryVO.getMistlxFpgaon(); 
        mistlxTotalMin = deviceTelemetryVO.getMistlxTotalMin(); 
        mistlxMincnt = deviceTelemetryVO.getMistlxMincnt();
        mistlxFailcnt = deviceTelemetryVO.getMistlxFailcnt(); 
        mistlxCtime = deviceTelemetryVO.getMistlxCtime(); 
        mistlxCtimeMod = deviceTelemetryVO.getMistlxCtimeMod();
        siteName = deviceTelemetryVO.getSiteName();
        areaName = deviceTelemetryVO.getAreaName();
        assetName = deviceTelemetryVO.getAssetName();
        pointLocationName = deviceTelemetryVO.getPointLocationName();
        pointName = deviceTelemetryVO.getPointName();
        baseStationPort = deviceTelemetryVO.getBaseStationPort();
        baseStationMac = deviceTelemetryVO.getBaseStationMac();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getBaseStationHostname() {
        return baseStationHostname;
    }

    public void setBaseStationHostname(String baseStationHostname) {
        this.baseStationHostname = baseStationHostname;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getEchoFw() {
        return echoFw;
    }

    public void setEchoFw(String echoFw) {
        this.echoFw = echoFw;
    }

    public short getEchoDatasets() {
        return echoDatasets;
    }

    public void setEchoDatasets(short echoDatasets) {
        this.echoDatasets = echoDatasets;
    }

    public short getEchoBacklog() {
        return echoBacklog;
    }

    public void setEchoBacklog(short echoBacklog) {
        this.echoBacklog = echoBacklog;
    }

    public byte getEchoRadioFail() {
        return echoRadioFail;
    }

    public void setEchoRadioFail(byte echoRadioFail) {
        this.echoRadioFail = echoRadioFail;
    }

    public byte getEchoTerr() {
        return echoTerr;
    }

    public void setEchoTerr(byte echoTerr) {
        this.echoTerr = echoTerr;
    }

    public short getEchoRestart() {
        return echoRestart;
    }

    public void setEchoRestart(short echoRestart) {
        this.echoRestart = echoRestart;
    }

    public byte getEchoPowLevel() {
        return echoPowLevel;
    }

    public void setEchoPowLevel(byte echoPowLevel) {
        this.echoPowLevel = echoPowLevel;
    }

    public short getEchoReboot() {
        return echoReboot;
    }

    public void setEchoReboot(short echoReboot) {
        this.echoReboot = echoReboot;
    }

    public short getEchoPowerfault() {
        return echoPowerfault;
    }

    public void setEchoPowerfault(short echoPowerfault) {
        this.echoPowerfault = echoPowerfault;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public byte getMistlxChan() {
        return mistlxChan;
    }

    public void setMistlxChan(byte mistlxChan) {
        this.mistlxChan = mistlxChan;
    }

    public byte getMistlxRssi() {
        return mistlxRssi;
    }

    public void setMistlxRssi(byte mistlxRssi) {
        this.mistlxRssi = mistlxRssi;
    }

    public byte getMistlxFsamp() {
        return mistlxFsamp;
    }

    public void setMistlxFsamp(byte mistlxFsamp) {
        this.mistlxFsamp = mistlxFsamp;
    }

    public short getMistlxLength() {
        return mistlxLength;
    }

    public void setMistlxLength(short mistlxLength) {
        this.mistlxLength = mistlxLength;
    }

    public float getMistlxBattery() {
        return mistlxBattery;
    }

    public void setMistlxBattery(float mistlxBattery) {
        this.mistlxBattery = mistlxBattery;
    }

    public float getMistlxTemp() {
        return mistlxTemp;
    }

    public void setMistlxTemp(float mistlxTemp) {
        this.mistlxTemp = mistlxTemp;
    }

    public short getMistlxDelta() {
        return mistlxDelta;
    }

    public void setMistlxDelta(short mistlxDelta) {
        this.mistlxDelta = mistlxDelta;
    }

    public short getMistlxPcorr() {
        return mistlxPcorr;
    }

    public void setMistlxPcorr(short mistlxPcorr) {
        this.mistlxPcorr = mistlxPcorr;
    }

    public byte getMistlxDbg() {
        return mistlxDbg;
    }

    public void setMistlxDbg(byte mistlxDbg) {
        this.mistlxDbg = mistlxDbg;
    }

    public byte getMistlxBleon() {
        return mistlxBleon;
    }

    public void setMistlxBleon(byte mistlxBleon) {
        this.mistlxBleon = mistlxBleon;
    }

    public byte getMistlxFpgaon() {
        return mistlxFpgaon;
    }

    public void setMistlxFpgaon(byte mistlxFpgaon) {
        this.mistlxFpgaon = mistlxFpgaon;
    }

    public short getMistlxTotalMin() {
        return mistlxTotalMin;
    }

    public void setMistlxTotalMin(short mistlxTotalMin) {
        this.mistlxTotalMin = mistlxTotalMin;
    }

    public short getMistlxMincnt() {
        return mistlxMincnt;
    }

    public void setMistlxMincnt(short mistlxMincnt) {
        this.mistlxMincnt = mistlxMincnt;
    }

    public short getMistlxFailcnt() {
        return mistlxFailcnt;
    }

    public void setMistlxFailcnt(short mistlxFailcnt) {
        this.mistlxFailcnt = mistlxFailcnt;
    }

    public short getMistlxCtime() {
        return mistlxCtime;
    }

    public void setMistlxCtime(short mistlxCtime) {
        this.mistlxCtime = mistlxCtime;
    }

    public short getMistlxCtimeMod() {
        return mistlxCtimeMod;
    }

    public void setMistlxCtimeMod(short mistlxCtimeMod) {
        this.mistlxCtimeMod = mistlxCtimeMod;
    }

    public Instant getLastSample() {
        return lastSample;
    }

    public void setLastSample(Instant lastSample) {
        this.lastSample = lastSample;
    }

    public String getEchoDeviceId() {
        return echoDeviceId;
    }

    public void setEchoDeviceId(String echoDeviceId) {
        this.echoDeviceId = echoDeviceId;
    }

    public String getMistDeviceId() {
        return mistDeviceId;
    }

    public void setMistDeviceId(String mistDeviceId) {
        this.mistDeviceId = mistDeviceId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getBaseStationMac() {
        return baseStationMac;
    }

    public void setBaseStationMac(String baseStationMac) {
        this.baseStationMac = baseStationMac;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.baseStationHostname);
        hash = 97 * hash + Objects.hashCode(this.deviceId);
        hash = 97 * hash + Objects.hashCode(this.deviceType);
        hash = 97 * hash + Objects.hashCode(this.echoFw);
        hash = 97 * hash + this.echoDatasets;
        hash = 97 * hash + this.echoBacklog;
        hash = 97 * hash + this.echoRadioFail;
        hash = 97 * hash + this.echoTerr;
        hash = 97 * hash + this.echoRestart;
        hash = 97 * hash + this.echoPowLevel;
        hash = 97 * hash + this.echoReboot;
        hash = 97 * hash + this.echoPowerfault;
        hash = 97 * hash + Objects.hashCode(this.lastUpdate);
        hash = 97 * hash + this.mistlxChan;
        hash = 97 * hash + Objects.hashCode(this.lastSample);
        hash = 97 * hash + this.mistlxRssi;
        hash = 97 * hash + this.mistlxFsamp;
        hash = 97 * hash + this.mistlxLength;
        hash = 97 * hash + Float.floatToIntBits(this.mistlxBattery);
        hash = 97 * hash + Float.floatToIntBits(this.mistlxTemp);
        hash = 97 * hash + this.mistlxDelta;
        hash = 97 * hash + this.mistlxPcorr;
        hash = 97 * hash + this.mistlxDbg;
        hash = 97 * hash + this.mistlxBleon;
        hash = 97 * hash + this.mistlxFpgaon;
        hash = 97 * hash + this.mistlxTotalMin;
        hash = 97 * hash + this.mistlxMincnt;
        hash = 97 * hash + this.mistlxFailcnt;
        hash = 97 * hash + this.mistlxCtime;
        hash = 97 * hash + this.mistlxCtimeMod;
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.areaName);
        hash = 97 * hash + Objects.hashCode(this.assetName);
        hash = 97 * hash + Objects.hashCode(this.pointLocationName);
        hash = 97 * hash + Objects.hashCode(this.pointName);
        hash = 97 * hash + this.baseStationPort;
        hash = 97 * hash + Objects.hashCode(this.baseStationMac);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceTelemetryVO other = (DeviceTelemetryVO) obj;
        if (this.echoDatasets != other.echoDatasets) {
            return false;
        }
        if (this.echoBacklog != other.echoBacklog) {
            return false;
        }
        if (this.echoRadioFail != other.echoRadioFail) {
            return false;
        }
        if (this.echoTerr != other.echoTerr) {
            return false;
        }
        if (this.echoRestart != other.echoRestart) {
            return false;
        }
        if (this.echoPowLevel != other.echoPowLevel) {
            return false;
        }
        if (this.echoReboot != other.echoReboot) {
            return false;
        }
        if (this.echoPowerfault != other.echoPowerfault) {
            return false;
        }
        if (this.mistlxChan != other.mistlxChan) {
            return false;
        }
        if (this.mistlxRssi != other.mistlxRssi) {
            return false;
        }
        if (this.mistlxFsamp != other.mistlxFsamp) {
            return false;
        }
        if (this.mistlxLength != other.mistlxLength) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxBattery) != Float.floatToIntBits(other.mistlxBattery)) {
            return false;
        }
        if (Float.floatToIntBits(this.mistlxTemp) != Float.floatToIntBits(other.mistlxTemp)) {
            return false;
        }
        if (this.mistlxDelta != other.mistlxDelta) {
            return false;
        }
        if (this.mistlxPcorr != other.mistlxPcorr) {
            return false;
        }
        if (this.mistlxDbg != other.mistlxDbg) {
            return false;
        }
        if (this.mistlxBleon != other.mistlxBleon) {
            return false;
        }
        if (this.mistlxFpgaon != other.mistlxFpgaon) {
            return false;
        }
        if (this.mistlxTotalMin != other.mistlxTotalMin) {
            return false;
        }
        if (this.mistlxMincnt != other.mistlxMincnt) {
            return false;
        }
        if (this.mistlxFailcnt != other.mistlxFailcnt) {
            return false;
        }
        if (this.mistlxCtime != other.mistlxCtime) {
            return false;
        }
        if (this.mistlxCtimeMod != other.mistlxCtimeMod) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.deviceType, other.deviceType)) {
            return false;
        }
        if (!Objects.equals(this.echoFw, other.echoFw)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.lastUpdate, other.lastUpdate)) {
            return false;
        }
        if (!Objects.equals(this.lastSample, other.lastSample)) {
            return false;
        }
        if (!Objects.equals(this.baseStationHostname, other.baseStationHostname)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.baseStationPort, other.baseStationPort)) {
            return false;
        }
        if (!Objects.equals(this.baseStationMac, other.baseStationMac)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DeviceTelemetryVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", baseStationHostname=" + baseStationHostname +", deviceId=" + deviceId + ", echoDeviceId=" + echoDeviceId + ", mistDeviceId=" + mistDeviceId + ", deviceType=" + deviceType + ", echoFw=" + echoFw + ", echoDatasets=" + echoDatasets + ", echoBacklog=" + echoBacklog + ", echoRadioFail=" + echoRadioFail + ", echoTerr=" + echoTerr + ", echoRestart=" + echoRestart + ", echoPowLevel=" + echoPowLevel + ", echoReboot=" + echoReboot + ", echoPowerfault=" + echoPowerfault + ", lastUpdate=" + lastUpdate + ", mistlxChan=" + mistlxChan + ", lastSample=" + lastSample + ", mistlxRssi=" + mistlxRssi + ", mistlxFsamp=" + mistlxFsamp + ", mistlxLength=" + mistlxLength + ", mistlxBattery=" + mistlxBattery + ", mistlxTemp=" + mistlxTemp + ", mistlxDelta=" + mistlxDelta + ", mistlxPcorr=" + mistlxPcorr + ", mistlxDbg=" + mistlxDbg + ", mistlxBleon=" + mistlxBleon + ", mistlxFpgaon=" + mistlxFpgaon + ", mistlxTotalMin=" + mistlxTotalMin + ", mistlxMincnt=" + mistlxMincnt + ", mistlxFailcnt=" + mistlxFailcnt + ", mistlxCtime=" + mistlxCtime + ", mistlxCtimeMod=" + mistlxCtimeMod + ", siteName=" + siteName + ", areaName=" + areaName +", assetName=" + assetName +", pointLocationName=" + pointLocationName +", pointName=" + pointName +", baseStationPort=" + baseStationPort +", baseStationMac=" + baseStationMac +'}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import com.uptime.monitoring.vo.GatewayLastUploadVO;
import static com.uptime.services.util.HttpUtils.parseInstant;
import com.uptime.monitoring.vo.MonitorVO;
import com.uptime.monitoring.vo.SiteDeviceBasestationVO;
import static com.uptime.services.util.JsonConverterUtil.DATE_TIME_OFFSET_FORMAT;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelegateUtil.class.getName());

    /**
     * Parse the given json String Object and return a MonitorVO object
     *
     * @param content, String Object
     * @return MonitorVO Object
     */
    public static MonitorVO parser(String content) {
        MonitorVO monitoringVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            monitoringVO = new MonitorVO();
            if (jsonObject.has("customerAccount")) {
                try {
                    monitoringVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("siteId")) {
                try {
                    monitoringVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("macAddress")) {
                try {
                    monitoringVO.setMacAddress(jsonObject.get("macAddress").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("issuedToken")) {
                try {
                    monitoringVO.setIssuedToken(UUID.fromString(jsonObject.get("issuedToken").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("passcode")) {
                try {
                    monitoringVO.setPasscode(UUID.fromString(jsonObject.get("passcode").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("lastPasscode")) {
                try {
                    monitoringVO.setLastPasscode(LocalDateTime.parse(jsonObject.get("lastPasscode").getAsString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
        }
        return monitoringVO;
    }

    /**
     * Parse the given json String Object and return a DeviceTelemetryVO object
     *
     * @param content, String Object
     * @return MonitorVO Object
     */
    public static DeviceTelemetryVO dTParser(String content) {
        DeviceTelemetryVO deviceTelemetryVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            deviceTelemetryVO = new DeviceTelemetryVO();
            if (jsonObject.has("customerAccount")) {
                try {
                    deviceTelemetryVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("customer_acct")) {
                try {
                    deviceTelemetryVO.setCustomerAccount(jsonObject.get("customer_acct").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("siteId")) {
                try {
                    deviceTelemetryVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("site_id")) {
                try {
                    deviceTelemetryVO.setSiteId(UUID.fromString(jsonObject.get("site_id").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoDeviceId")) {
                try {
                    deviceTelemetryVO.setEchoDeviceId(jsonObject.get("echoDeviceId").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_device_id")) {
                try {
                    deviceTelemetryVO.setEchoDeviceId(jsonObject.get("echo_device_id").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }

            if (jsonObject.has("mistDeviceId")) {
                try {
                    deviceTelemetryVO.setMistDeviceId(jsonObject.get("mistDeviceId").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mist_device_id")) {
                try {
                    deviceTelemetryVO.setMistDeviceId(jsonObject.get("mist_device_id").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("deviceType")) {
                try {
                    deviceTelemetryVO.setDeviceType(jsonObject.get("deviceType").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoFw")) {
                try {
                    deviceTelemetryVO.setEchoFw(jsonObject.get("echoFw").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_fw")) {
                try {
                    deviceTelemetryVO.setEchoFw(jsonObject.get("echo_fw").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoDatasets")) {
                try {
                    deviceTelemetryVO.setEchoDatasets(jsonObject.get("echoDatasets").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_datasets")) {
                try {
                    deviceTelemetryVO.setEchoDatasets(jsonObject.get("echo_datasets").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoBacklog")) {
                try {
                    deviceTelemetryVO.setEchoBacklog(jsonObject.get("echoBacklog").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_backlog")) {
                try {
                    deviceTelemetryVO.setEchoBacklog(jsonObject.get("echo_backlog").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoRadioFail")) {
                try {
                    deviceTelemetryVO.setEchoRadioFail(jsonObject.get("echoRadioFail").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_radio_fail")) {
                try {
                    deviceTelemetryVO.setEchoRadioFail(jsonObject.get("echo_radio_fail").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoTerr")) {
                try {
                    deviceTelemetryVO.setEchoTerr(jsonObject.get("echoTerr").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_terr")) {
                try {
                    deviceTelemetryVO.setEchoTerr(jsonObject.get("echo_terr").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoRestart")) {
                try {
                    deviceTelemetryVO.setEchoRestart(jsonObject.get("echoRestart").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_restart")) {
                try {
                    deviceTelemetryVO.setEchoRestart(jsonObject.get("echo_restart").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoPowLevel")) {
                try {
                    deviceTelemetryVO.setEchoPowLevel(jsonObject.get("echoPowLevel").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_pow_level")) {
                try {
                    deviceTelemetryVO.setEchoPowLevel(jsonObject.get("echo_pow_level").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoReboot")) {
                try {
                    deviceTelemetryVO.setEchoReboot(jsonObject.get("echoReboot").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_reboot")) {
                try {
                    deviceTelemetryVO.setEchoReboot(jsonObject.get("echo_reboot").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echoPowerfault")) {
                try {
                    deviceTelemetryVO.setEchoPowerfault(jsonObject.get("echoPowerfault").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("echo_powerfault")) {
                try {
                    deviceTelemetryVO.setEchoPowerfault(jsonObject.get("echo_powerfault").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("lastUpdate")) {
                try {
                    deviceTelemetryVO.setLastUpdate(LocalDateTime.parse(jsonObject.get("lastUpdate").getAsString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("last_update")) {
                try {
                    deviceTelemetryVO.setLastUpdate(LocalDateTime.parse(jsonObject.get("last_update").getAsString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxChan")) {
                try {
                    deviceTelemetryVO.setMistlxChan(jsonObject.get("mistlxChan").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_chan")) {
                try {
                    deviceTelemetryVO.setMistlxChan(jsonObject.get("mistlx_chan").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("lastSample")) {
                try {
                    deviceTelemetryVO.setLastSample(parseInstant(jsonObject.get("lastSample").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("last_sample")) {
                try {
                    deviceTelemetryVO.setLastSample(parseInstant(jsonObject.get("last_sample").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxRssi")) {
                try {
                    deviceTelemetryVO.setMistlxRssi(jsonObject.get("mistlxRssi").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_rssi")) {
                try {
                    deviceTelemetryVO.setMistlxRssi(jsonObject.get("mistlx_rssi").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxFsamp")) {
                try {
                    deviceTelemetryVO.setMistlxFsamp(jsonObject.get("mistlxFsamp").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_fsamp")) {
                try {
                    deviceTelemetryVO.setMistlxFsamp(jsonObject.get("mistlx_fsamp").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxLength")) {
                try {
                    deviceTelemetryVO.setMistlxLength(jsonObject.get("mistlxLength").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_length")) {
                try {
                    deviceTelemetryVO.setMistlxLength(jsonObject.get("mistlx_length").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxBattery")) {
                try {
                    deviceTelemetryVO.setMistlxBattery(jsonObject.get("mistlxBattery").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_battery")) {
                try {
                    deviceTelemetryVO.setMistlxBattery(jsonObject.get("mistlx_battery").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxTemp")) {
                try {
                    deviceTelemetryVO.setMistlxTemp(jsonObject.get("mistlxTemp").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_temp")) {
                try {
                    deviceTelemetryVO.setMistlxTemp(jsonObject.get("mistlx_temp").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxDelta")) {
                try {
                    deviceTelemetryVO.setMistlxDelta(jsonObject.get("mistlxDelta").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_delta")) {
                try {
                    deviceTelemetryVO.setMistlxDelta(jsonObject.get("mistlx_delta").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxPcorr")) {
                try {
                    deviceTelemetryVO.setMistlxPcorr(jsonObject.get("mistlxPcorr").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_pcorr")) {
                try {
                    deviceTelemetryVO.setMistlxPcorr(jsonObject.get("mistlx_pcorr").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxDbg")) {
                try {
                    deviceTelemetryVO.setMistlxDbg(jsonObject.get("mistlxDbg").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_dbg")) {
                try {
                    deviceTelemetryVO.setMistlxDbg(jsonObject.get("mistlx_dbg").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxBleon")) {
                try {
                    deviceTelemetryVO.setMistlxBleon(jsonObject.get("mistlxBleon").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_bleon")) {
                try {
                    deviceTelemetryVO.setMistlxBleon(jsonObject.get("mistlx_bleon").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxFpgaon")) {
                try {
                    deviceTelemetryVO.setMistlxFpgaon(jsonObject.get("mistlxFpgaon").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_fpgaon")) {
                try {
                    deviceTelemetryVO.setMistlxFpgaon(jsonObject.get("mistlx_fpgaon").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxTotalMin")) {
                try {
                    deviceTelemetryVO.setMistlxTotalMin(jsonObject.get("mistlxTotalMin").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_total_min")) {
                try {
                    deviceTelemetryVO.setMistlxTotalMin(jsonObject.get("mistlx_total_min").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxMincnt")) {
                try {
                    deviceTelemetryVO.setMistlxMincnt(jsonObject.get("mistlxMincnt").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_mincnt")) {
                try {
                    deviceTelemetryVO.setMistlxMincnt(jsonObject.get("mistlx_mincnt").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxFailcnt")) {
                try {
                    deviceTelemetryVO.setMistlxFailcnt(jsonObject.get("mistlxFailcnt").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_failcnt")) {
                try {
                    deviceTelemetryVO.setMistlxFailcnt(jsonObject.get("mistlx_failcnt").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxCtime")) {
                try {
                    deviceTelemetryVO.setMistlxCtime(jsonObject.get("mistlxCtime").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_ctime")) {
                try {
                    deviceTelemetryVO.setMistlxCtime(jsonObject.get("mistlx_ctime").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlxCtimeMod")) {
                try {
                    deviceTelemetryVO.setMistlxCtimeMod(jsonObject.get("mistlxCtimeMod").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("mistlx_ctime_mod")) {
                try {
                    deviceTelemetryVO.setMistlxCtimeMod(jsonObject.get("mistlx_ctime_mod").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("siteName")) {
                try {
                    deviceTelemetryVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("site_name")) {
                try {
                    deviceTelemetryVO.setSiteName(jsonObject.get("site_name").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("areaName")) {
                try {
                    deviceTelemetryVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("area_name")) {
                try {
                    deviceTelemetryVO.setAreaName(jsonObject.get("area_name").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("assetName")) {
                try {
                    deviceTelemetryVO.setAssetName(jsonObject.get("assetName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("asset_name")) {
                try {
                    deviceTelemetryVO.setAssetName(jsonObject.get("asset_name").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("pointLocationName")) {
                try {
                    deviceTelemetryVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("point_location_name")) {
                try {
                    deviceTelemetryVO.setPointLocationName(jsonObject.get("point_location_name").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("pointName")) {
                try {
                    deviceTelemetryVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("point_name")) {
                try {
                    deviceTelemetryVO.setPointName(jsonObject.get("point_name").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("baseStationMac")) {
                try {
                    deviceTelemetryVO.setBaseStationMac(jsonObject.get("baseStationMac").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("base_station_mac")) {
                try {
                    deviceTelemetryVO.setBaseStationMac(jsonObject.get("base_station_mac").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("baseStationPort")) {
                try {
                    deviceTelemetryVO.setBaseStationPort(jsonObject.get("baseStationPort").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("base_station_port")) {
                try {
                    deviceTelemetryVO.setBaseStationPort(jsonObject.get("base_station_port").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("baseStationHostName")) {
                try {
                    deviceTelemetryVO.setBaseStationHostname(jsonObject.get("base_station_hostname").getAsString());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("base_station_hostname")) {
                try {
                    deviceTelemetryVO.setBaseStationHostname(jsonObject.get("base_station_hostname").getAsString());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
        }
        return deviceTelemetryVO;
    }

    /**
     * Parse the given json String Object and return a SiteDeviceBasestationVO object
     *
     * @param content, String Object
     * @return SiteDeviceBasestationVO Object
     */
    public static SiteDeviceBasestationVO siteDeviceBasestationVOParser(String content) {
        SiteDeviceBasestationVO siteDeviceBasestationVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            siteDeviceBasestationVO = new SiteDeviceBasestationVO();
            if (jsonObject.has("customer_acct")) {
                try {
                    siteDeviceBasestationVO.setCustomerAccount(jsonObject.get("customer_acct").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("site_id")) {
                try {
                    siteDeviceBasestationVO.setSiteId(UUID.fromString(jsonObject.get("site_id").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("base_station_port")) {
                try {
                    siteDeviceBasestationVO.setBaseStationPort(jsonObject.get("base_station_port").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("device_id")) {
                try {
                    siteDeviceBasestationVO.setDeviceId(jsonObject.get("device_id").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }

            if (jsonObject.has("point_id")) {
                try {
                    siteDeviceBasestationVO.setPointId(UUID.fromString(jsonObject.get("point_id").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("channel_type")) {
                try {
                    siteDeviceBasestationVO.setChannelType(jsonObject.get("channel_type").getAsString().trim().toUpperCase());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
            if (jsonObject.has("fmax")) {
                try {
                    siteDeviceBasestationVO.setFmax(jsonObject.get("fmax").getAsInt());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    System.out.println("fmax Exception - " + ex.toString());
                }
            }
            if (jsonObject.has("resolution")) {
                try {
                    siteDeviceBasestationVO.setResolution(jsonObject.get("resolution").getAsInt());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    System.out.println("resolution Exception - " + ex.toString());
                }
            }

            if (jsonObject.has("sample_interval")) {
                try {
                    siteDeviceBasestationVO.setSampleInterval(jsonObject.get("sample_interval").getAsShort());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    System.out.println("sample_interval Exception - " + ex.toString());
                }
            }

            if (jsonObject.has("sensor_channel_num")) {
                try {
                    siteDeviceBasestationVO.setSensorChannelNum(jsonObject.get("sensor_channel_num").getAsByte());
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                    System.out.println("sensor_channel_num Exception - " + ex.toString());
                }
            }

            if (jsonObject.has("last_sampled")) {
                try {
                    siteDeviceBasestationVO.setLastSampled(parseInstant(jsonObject.get("last_sampled").getAsString()));
                } catch (Exception ex) {
                    LOGGER.warn(ex.getMessage(), ex);
                }
            }
        }
        return siteDeviceBasestationVO;
    }

    /**
     * Parse the given json String Object and return a List Object of GatewayLastUploadVO Objects
     *
     * @param content, String Object
     * @return List Object of GatewayLastUploadVO Objects
     */
    public static List<GatewayLastUploadVO> gatewayLastUploadVOParser(String content) {
        List<GatewayLastUploadVO> gatewayLastUploadList = null;
        GatewayLastUploadVO gatewayLastUploadVO;
        String customerAccount, siteName;
        UUID siteId;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        try {
            if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                gatewayLastUploadList = new ArrayList();
                customerAccount = null; 
                siteName = null;
                siteId = null;
                
                if (jsonObject.has("customer_acct")) {
                    customerAccount = jsonObject.get("customer_acct").getAsString().trim().toUpperCase();
                }
                
                if (jsonObject.has("site_id")) {
                    siteId = UUID.fromString(jsonObject.get("site_id").getAsString());
                }
                
                if (jsonObject.has("site_name")) {
                    siteName = jsonObject.get("site_name").getAsString().trim().toUpperCase();
                }
                
                if (jsonObject.has("gateways")) {
                    if ((jsonArray = jsonObject.getAsJsonArray("gateways")) != null && !jsonArray.isEmpty()) {
                        gatewayLastUploadList = new ArrayList();

                        for (int i = 0; i < jsonArray.size(); i++) {
                            gatewayLastUploadVO = new GatewayLastUploadVO();
                            jsonObject = jsonArray.get(i).getAsJsonObject();
                            
                            gatewayLastUploadVO.setCustomerAccount(customerAccount);
                            gatewayLastUploadVO.setSiteId(siteId);
                            gatewayLastUploadVO.setSiteName(siteName);
                            
                            try {
                                if (jsonObject.has("gateway_id")) {
                                    gatewayLastUploadVO.setDeviceId(jsonObject.get("gateway_id").getAsString().trim().toUpperCase());
                                }

                                if (jsonObject.has("last_upload")) {
                                    gatewayLastUploadVO.setLastUpload(parseInstant(jsonObject.get("last_upload").getAsString()));
                                }

                                gatewayLastUploadList.add(gatewayLastUploadVO);
                            } catch (Exception ex) {}
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.warn(ex.getMessage(), ex);
            gatewayLastUploadList = null;
        }
        return gatewayLastUploadList;
    }

    public static void main(String[] args) {
        String json = "{\n"
                + "  \"customer_acct\": \"90000\",\n"
                + "  \"site_id\": \"187e4953-a9cc-47f1-a8be-0b98fd85cd9a\",\n"
                + "  \"echo_device_id\": \"0xbc000333\",\n"
                + "  \"echo_fw\": \"57\",\n"
                + "  \"echo_datasets\": 19,\n"
                + "  \"echo_backlog\": 6,\n"
                + "  \"echo_radio_fail\": 0,\n"
                + "  \"echo_terr\": 2,\n"
                + "  \"echo_restart\": 6,\n"
                + "  \"echo_pow_level\": 6,\n"
                + "  \"echo_reboot\": 16,\n"
                + "  \"echo_powerfault\": 0,\n"
                + "  \"last_update\": \"2022-04-02T18:26:00-0500\"\n"
                + "}";
        JsonUtil.dTParser(json);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.utils;

import com.uptime.cassandra.monitor.entity.ApprovedBaseStations;
import com.uptime.cassandra.monitor.entity.EchoDiagnostics;
import com.uptime.cassandra.monitor.entity.GatewayLastUpload;
import com.uptime.cassandra.monitor.entity.MistDiagnostics;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import com.uptime.monitoring.vo.GatewayLastUploadVO;
import com.uptime.monitoring.vo.MonitorVO;
import com.uptime.monitoring.vo.SiteDeviceBasestationVO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gsingh
 */
public class DelegateUtil {

    /**
     * Convert MonitorVO Object to ApprovedBaseStations Object
     *
     * @param monitoringVO, MonitorVO Object
     * @return ApprovedBaseStations Object
     */
    public static ApprovedBaseStations getApprovedBaseStations(MonitorVO monitoringVO) {
        ApprovedBaseStations entity = new ApprovedBaseStations();

        if (monitoringVO != null) {
            entity.setCustomerAccount(monitoringVO.getCustomerAccount());
            entity.setSiteId(monitoringVO.getSiteId());
            entity.setMacAddress(monitoringVO.getMacAddress());
            entity.setIssuedToken(monitoringVO.getIssuedToken());
            entity.setPasscode(monitoringVO.getPasscode());
            entity.setLastPasscode(monitoringVO.getLastPasscode());
        }
        return entity;
    }

    /**
     * Convert DeviceTelemetryVO Object to MistDiagnostics Object
     *
     * @param deviceTelemetryVO, DeviceTelemetryVO Object
     * @return MistDiagnostics Object
     */
    public static MistDiagnostics getMistDiagnostics(DeviceTelemetryVO deviceTelemetryVO) {
        MistDiagnostics entity = new MistDiagnostics();

        if (deviceTelemetryVO != null) {
            entity.setCustomerAccount(deviceTelemetryVO.getCustomerAccount());
            entity.setSiteId(deviceTelemetryVO.getSiteId());
            entity.setEchoDeviceId(deviceTelemetryVO.getEchoDeviceId());
            entity.setDeviceId(deviceTelemetryVO.getMistDeviceId());
            entity.setMistlxChan(deviceTelemetryVO.getMistlxChan());
            entity.setLastSample(deviceTelemetryVO.getLastSample());
            entity.setMistlxRssi(deviceTelemetryVO.getMistlxRssi());
            entity.setMistlxFsamp(deviceTelemetryVO.getMistlxFsamp());
            entity.setMistlxLength(deviceTelemetryVO.getMistlxLength());
            entity.setMistlxBattery(deviceTelemetryVO.getMistlxBattery());
            entity.setMistlxTemp(deviceTelemetryVO.getMistlxTemp());
            entity.setMistlxDelta(deviceTelemetryVO.getMistlxDelta());
            entity.setMistlxPcorr(deviceTelemetryVO.getMistlxPcorr());
            entity.setMistlxDbg(deviceTelemetryVO.getMistlxDbg());
            entity.setMistlxBleon(deviceTelemetryVO.getMistlxBleon());
            entity.setMistlxFpgaon(deviceTelemetryVO.getMistlxFpgaon());
            entity.setMistlxTotalMin(deviceTelemetryVO.getMistlxTotalMin());
            entity.setMistlxMincnt(deviceTelemetryVO.getMistlxMincnt());
            entity.setMistlxFailcnt(deviceTelemetryVO.getMistlxFailcnt());
            entity.setMistlxCtime(deviceTelemetryVO.getMistlxCtime());
            entity.setMistlxCtimeMod(deviceTelemetryVO.getMistlxCtimeMod());
            entity.setAreaName(deviceTelemetryVO.getAreaName());
            entity.setAssetName(deviceTelemetryVO.getAssetName());
            entity.setBaseStationMac(deviceTelemetryVO.getBaseStationMac());
            entity.setBaseStationPort(deviceTelemetryVO.getBaseStationPort());
            entity.setPointLocationName(deviceTelemetryVO.getPointLocationName());
            entity.setPointName(deviceTelemetryVO.getPointName());
            entity.setSiteName(deviceTelemetryVO.getSiteName());
            entity.setBaseStationHostName(deviceTelemetryVO.getBaseStationHostname());
        }
        return entity;
    }

    /**
     * Convert DeviceTelemetryVO Object to EchoDiagnostics Object
     *
     * @param deviceTelemetryVO, DeviceTelemetryVO Object
     * @return EchoDiagnostics Object
     */
    public static EchoDiagnostics getEchoDiagnostics(DeviceTelemetryVO deviceTelemetryVO) {
        EchoDiagnostics entity = new EchoDiagnostics();

        if (deviceTelemetryVO != null) {
            entity.setCustomerAccount(deviceTelemetryVO.getCustomerAccount());
            entity.setSiteId(deviceTelemetryVO.getSiteId());
            entity.setBaseStationHostname(deviceTelemetryVO.getBaseStationHostname());
            entity.setDeviceId(deviceTelemetryVO.getEchoDeviceId());
            entity.setEchoFw(deviceTelemetryVO.getEchoFw());
            entity.setEchoDatasets(deviceTelemetryVO.getEchoDatasets());
            entity.setEchoBacklog(deviceTelemetryVO.getEchoBacklog());
            entity.setEchoRadioFail(deviceTelemetryVO.getEchoRadioFail());
            entity.setEchoTerr(deviceTelemetryVO.getEchoTerr());
            entity.setEchoRestart(deviceTelemetryVO.getEchoRestart());
            entity.setEchoPowLevel(deviceTelemetryVO.getEchoPowLevel());
            entity.setEchoReboot(deviceTelemetryVO.getEchoReboot());
            entity.setEchoPowerfault(deviceTelemetryVO.getEchoPowerfault());
            entity.setLastUpdate(deviceTelemetryVO.getLastUpdate());
            entity.setBaseStationMac(deviceTelemetryVO.getBaseStationMac());
            entity.setSiteName(deviceTelemetryVO.getSiteName());
        }
        return entity;
    }

    /**
     * Convert SiteDeviceBasestationVO Object to SiteDeviceBasestation Object
     *
     * @param siteDeviceBasestationVO, SiteDeviceBasestationVO Object
     * @return SiteDeviceBasestation Object
     */
    public static SiteDeviceBasestation getSiteDeviceBasestation(SiteDeviceBasestationVO siteDeviceBasestationVO) {
        SiteDeviceBasestation entity = new SiteDeviceBasestation();

        if (siteDeviceBasestationVO != null) {
            entity.setCustomerAccount(siteDeviceBasestationVO.getCustomerAccount());
            entity.setSiteId(siteDeviceBasestationVO.getSiteId());
            entity.setDeviceId(siteDeviceBasestationVO.getDeviceId());
            entity.setBaseStationPort(siteDeviceBasestationVO.getBaseStationPort());
        }
        return entity;
    }

    /**
     * Convert SiteDeviceBasestationVO Object to SiteBasestationDevice Object
     *
     * @param siteDeviceBasestationVO, SiteDeviceBasestationVO Object
     * @return SiteBasestationDevice Object
     */
    public static SiteBasestationDevice getSiteBasestationDevice(SiteDeviceBasestationVO siteDeviceBasestationVO) {
        SiteBasestationDevice entity = new SiteBasestationDevice();

        if (siteDeviceBasestationVO != null) {
            entity.setCustomerAccount(siteDeviceBasestationVO.getCustomerAccount());
            entity.setSiteId(siteDeviceBasestationVO.getSiteId());
            entity.setDeviceId(siteDeviceBasestationVO.getDeviceId());
            entity.setBaseStationPort(siteDeviceBasestationVO.getBaseStationPort());
            entity.setPointId(siteDeviceBasestationVO.getPointId());
            entity.setFmax(siteDeviceBasestationVO.getFmax());
            entity.setChannelType(siteDeviceBasestationVO.getChannelType());
            entity.setLastSampled(siteDeviceBasestationVO.getLastSampled());
            entity.setResolution(siteDeviceBasestationVO.getResolution());
            entity.setSampleInterval(siteDeviceBasestationVO.getSampleInterval());
            entity.setSensorChannelNum(siteDeviceBasestationVO.getSensorChannelNum());
        }
        return entity;
    }

    /**
     * Convert List Object of GatewayLastUploadVO Objects to List Object of GatewayLastUpload Objects
     *
     * @param gatewayLastUploadVOList, List Object of GatewayLastUploadVO Objects
     * @return List Object of GatewayLastUpload Objects
     */
    public static List<GatewayLastUpload> getGatewayLastUploadList(List<GatewayLastUploadVO> gatewayLastUploadVOList) {
        List<GatewayLastUpload> entities = new ArrayList();
        GatewayLastUpload entity;
        
        if (gatewayLastUploadVOList != null) {
            for (GatewayLastUploadVO gatewayLastUploadVO : gatewayLastUploadVOList) {
                if (gatewayLastUploadVO != null) {
                    entity = new GatewayLastUpload();
                    entity.setCustomerAccount(gatewayLastUploadVO.getCustomerAccount());
                    entity.setSiteId(gatewayLastUploadVO.getSiteId());
                    entity.setSiteName(gatewayLastUploadVO.getSiteName());
                    entity.setDeviceId(gatewayLastUploadVO.getDeviceId());
                    entity.setLastUpload(gatewayLastUploadVO.getLastUpload());
                    entities.add(entity);
                }
            }
        }
        
        return entities;
    }
}

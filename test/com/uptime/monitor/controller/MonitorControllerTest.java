/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitor.controller;

import com.uptime.monitoring.controller.MonitorController;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MonitorControllerTest {
    private static  MonitorController instance;
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    @BeforeClass
    public static void setUpClass() {
        instance = new MonitorController();
    }

    /**
     * Test of createMonitor method, of class MonitorController.
     */
    @Test
    public void test10_CreateMonitor() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        
        String expResult = "{\"outcome\":\"New ApprovedBaseStations created successfully.\"}";
        String result = instance.createApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createApprovedBaseStations method, of class MonitorController.
     * SiteId not provided in Json
     */
    @Test
    public void test12_CreateApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    //"	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        
        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.createApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createApprovedBaseStations method, of class MonitorController.
     * Provided null Json
     */
    @Test
    public void test13_CreateApprovedBaseStations() {
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createApprovedBaseStations(null);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getMonitorByCustomerSite method, of class MonitorController.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_GetMonitorByCustomerSite() throws Exception {
        String customer = "77777";
        String site = "ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35";
        
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\",\"approvedBaseStations\":[{\"customerAccount\":\"77777\",\"siteId\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\",\"macAddress\":\"AA:BB:CC:DD:EE:FF\",\"issuedToken\":\"5be81920-a909-11ec-9929-ef33d37152d9\",\"lastPasscode\":\"2022-03-12T18:46:19+0000\",\"passcode\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getMonitorByCustomerSiteId(customer, site);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getMonitorByCustomerSite method, of class MonitorController.
     * Provided a siteId that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test22_GetMonitorByCustomerSite() throws Exception {
        String customer = "77777";
        String siteId = "" + UUID.randomUUID();
        String expResult = "{\"outcome\":\"No items found for the given info.\"}";
        String result = instance.getMonitorByCustomerSiteId(customer, siteId);
        System.out.println("result 22 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getMonitorByCustomerSite method, of class MonitorController.
     * Provided null siteId
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test23_GetMonitorByCustomerSite() throws Exception {
        String customer = "77777";
        fail(instance.getMonitorByCustomerSiteId(customer, null));
    }
    
    /**
     * Test of getMonitorByCustomerSite method, of class MonitorController.
     * Provided null customer account
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test24_GetMonitorByCustomerSite() throws Exception {
        fail(instance.getMonitorByCustomerSiteId(null, "ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35"));
    }
    
    /**
     * Test of getMonitorByCustomerSite method, of class MonitorController.
     * Provided invalid siteId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test25_GetMonitorByCustomerSite() throws Exception {
        String customer = "77777";
        fail(instance.getMonitorByCustomerSiteId(customer, "ac2bf5e0-e8c8-4895-b7fd"));
    }

    /**
     * Test of getMonitorByPK method, of class MonitorController.
     * @throws java.lang.Exception
     */
    @Test
    public void test31_GetMonitorByPK() throws Exception {
        String customer = "77777";
        String site = "ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35";
        String mac = "AA:BB:CC:DD:EE:FF";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\",\"macAddress\":\"AA:BB:CC:DD:EE:FF\",\"approvedBaseStations\":[{\"customerAccount\":\"77777\",\"siteId\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\",\"macAddress\":\"AA:BB:CC:DD:EE:FF\",\"issuedToken\":\"5be81920-a909-11ec-9929-ef33d37152d9\",\"lastPasscode\":\"2022-03-12T18:46:19+0000\",\"passcode\":\"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getMonitorByPK(customer, site, mac);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonitorByPK method, of class MonitorController.
     * @throws java.lang.Exception
     * Provided a siteId that does not exist
     */
    @Test
    public void test32_GetMonitorByPK() throws Exception {
        String customer = "77777";
        String site = "ac2bf5e0-e8c8-4895-b7fd-a44ff2660c36";
        String mac = "AA:BB:CC:DD:EE:FF";
        String expResult = "{\"outcome\":\"No items found for the given info.\"}";
        String result = instance.getMonitorByPK(customer, site, mac);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonitorByPK method, of class MonitorController.
     * @throws java.lang.Exception
     * Provided null siteId
     */
    @Test(expected = NullPointerException.class)
    public void test33_GetMonitorByPK() throws Exception {
        String customer = "77777";
        String site = null;
        String mac = "AA:BB:CC:DD:EE:FF";
        fail(instance.getMonitorByPK(customer, site, mac));
    }

    /**
     * Test of getMonitorByPK method, of class MonitorController.
     * @throws java.lang.Exception
     * Provided an invalid siteId
     */
    @Test(expected = IllegalArgumentException.class)
    public void test34_GetMonitorByPK() throws Exception {
        String customer = "77777";
        String site = "ac2bf5e0-e8c8";
        String mac = "AA:BB:CC:DD:EE:FF";
        fail(instance.getMonitorByPK(customer, site, mac));
    }
  
    /**
     * Test of updateApprovedBaseStations method, of class MonitorController.
     */
    @Test
    public void test41_UpdateApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        
        String expResult = "{\"outcome\":\"ApprovedBaseStations updated successfully.\"}";
        String result = instance.updateApprovedBaseStations(content);
        assertEquals(expResult, result);
    }
  
    /**
     * Test of updateApprovedBaseStations method, of class MonitorController.
     * Provided a siteId that does not exist
     */
    @Test
    public void test42_UpdateApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c36\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        
        String expResult = "{\"outcome\":\"ApprovedBaseStations updated successfully.\"}";
        String result = instance.updateApprovedBaseStations(content);
        assertEquals(expResult, result);
    }
  
    /**
     * Test of updateApprovedBaseStations method, of class MonitorController.
     * Provided a JSON without siteId 
     */
    @Test
    public void test43_UpdateApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    //"	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c36\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",   \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.updateApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApprovedBaseStations method, of class MonitorController.
     */
    @Test
    public void test51_DeleteApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        
        String expResult = "{\"outcome\":\"ApprovedBaseStations deleted successfully.\"}";
        String result = instance.deleteApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApprovedBaseStations method, of class MonitorController.
     * Provided a siteId that does not exist
     */
    @Test
    public void test52_DeleteApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c37\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"No ApprovedBaseStations found to delete.\"}";
        String result = instance.deleteApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApprovedBaseStations method, of class MonitorController.
     * Provided a JSON without siteId 
     */
    @Test
    public void test53_DeleteApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    //"	\"siteId\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c37\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.deleteApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApprovedBaseStations method, of class MonitorController.
     * Provided a JSON with invalid siteId 
     */
    @Test
    public void test54_DeleteApprovedBaseStations() {
        String content = "{\n" +
                    "	\"customerAccount\": \"77777\",     \n" +
                    "	\"siteId\": \"ac2bf5e0-e8c8-4895-\", \n" +
                    "	\"macAddress\": \"AA:BB:CC:DD:EE:FF\",             \n" +
                    "	\"issuedToken\": \"5be81920-a909-11ec-9929-ef33d37152d9\",            \n" +
                    "	\"lastPasscode\": \"2022-03-12T18:46:19-0700\",          \n" +
                    "	\"passcode\": \"ac2bf5e0-e8c8-4895-b7fd-a44ff2660c35\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.deleteApprovedBaseStations(content);
        assertEquals(expResult, result);
    }

}

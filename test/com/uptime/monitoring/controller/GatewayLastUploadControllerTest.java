/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GatewayLastUploadControllerTest {
    
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    private static GatewayLastUploadController instance;
    
    private static StringBuilder updateJson;
    private static Instant currentInstant;
    private static String currentInstantString;
    
    public GatewayLastUploadControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        updateJson = new StringBuilder();
        currentInstant = Instant.now();
        currentInstantString = LocalDateTime.ofInstant(currentInstant, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
        
        updateJson
            .append("{\"customer_acct\":\"TEST ACCOUNT\",")
            .append("\"site_id\":\"8b086e15-371f-4a8d-bedb-c1b67b49be31\",")
            .append("\"site_name\":\"TEST SITE\",")
            .append("\"gateways\":[{")
            .append("\"gateway_id\":\"BB000001\",")
            .append("\"last_upload\":\"").append(currentInstant.toString()).append("\"},")
            .append("{\"gateway_id\":\"BB000002\",")
            .append("\"last_upload\":\"").append(currentInstant.toString()).append("\"}]}");
        
        instance = new GatewayLastUploadController();
    }

    /**
     * Test of updateGatewayLastUpload method, of class GatewayLastUploadController.
     */
    @Test
    public void test0_UpdateGatewayLastUpload() {
        System.out.println("Start updateGatewayLastUpload...");
        assertEquals("{\"outcome\":\"ApprovedBaseStations updated successfully.\"}", instance.updateGatewayLastUpload(updateJson.toString()));
        System.out.println("updateGatewayLastUpload completed.");
    }

    /**
     * Test of getGatewayLastUploadByCustomerSiteId method, of class GatewayLastUploadController.
     */
    @Test
    public void test1_GetGatewayLastUploadByCustomerSiteId() {
        System.out.println("Start getGatewayLastUploadByCustomerSiteId...");
        String result;
        
        try {
            result = instance.getGatewayLastUploadByCustomerSiteId("TEST ACCOUNT", "8b086e15-371f-4a8d-bedb-c1b67b49be31");
            assertTrue(result.contains("BB000001"));
            assertTrue(result.contains("BB000002"));
            assertTrue(result.contains(currentInstantString));
            System.out.println("getGatewayLastUploadByCustomerSiteId completed.");
        } catch (Exception ex) {
            fail("getGatewayLastUploadByCustomerSiteId failed");
        }
    }

    /**
     * Test of getGatewayLastUploadByPK method, of class GatewayLastUploadController.
     */
    @Test
    public void test2_GetGatewayLastUploadByPK() {
        System.out.println("Start getGatewayLastUploadByPK...");
        String result;
        
        try {
            result = instance.getGatewayLastUploadByPK("TEST ACCOUNT", "8b086e15-371f-4a8d-bedb-c1b67b49be31","BB000001");
            assertTrue(result.contains(currentInstantString));
            System.out.println("getGatewayLastUploadByPK completed.");
        } catch (Exception ex) {
            fail("getGatewayLastUploadByPK failed");
        }
    }
    
}

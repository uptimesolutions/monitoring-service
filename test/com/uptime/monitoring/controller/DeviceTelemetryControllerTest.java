/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

//import com.uptime.monitoring.devicetelemetry.delegate.DeleteDeviceTelemetryDelegate;
import com.uptime.monitoring.vo.DeviceTelemetryVO;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeviceTelemetryControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    private static DeviceTelemetryController instance;
    
    private static DeviceTelemetryVO echoDiagnosticsVO;
    private static DeviceTelemetryVO mistDiagnosticsVO;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    
    public DeviceTelemetryControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new DeviceTelemetryController();
        echoDiagnosticsVO = new DeviceTelemetryVO();
        echoDiagnosticsVO.setCustomerAccount("77777");
        echoDiagnosticsVO.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
        echoDiagnosticsVO.setBaseStationHostname("Test Host");
        echoDiagnosticsVO.setBaseStationMac("Test MAC");
        echoDiagnosticsVO.setSiteName("Memphis");
        echoDiagnosticsVO.setEchoDeviceId("BB003003");
        echoDiagnosticsVO.setEchoBacklog((short) 0);
        echoDiagnosticsVO.setEchoDatasets((short) 1);
        echoDiagnosticsVO.setEchoFw("Test 11");
        echoDiagnosticsVO.setEchoPowLevel((byte) 0);
        echoDiagnosticsVO.setEchoPowerfault((short) 2);
        echoDiagnosticsVO.setEchoRadioFail((byte) 1);
        echoDiagnosticsVO.setEchoReboot((short) 3);
        echoDiagnosticsVO.setEchoRestart((short) 4);
        echoDiagnosticsVO.setEchoTerr((byte) 2);
        echoDiagnosticsVO.setLastUpdate(LocalDateTime.parse("2022-11-10 02:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        
        mistDiagnosticsVO = new DeviceTelemetryVO();
        mistDiagnosticsVO.setCustomerAccount("77777");
        mistDiagnosticsVO.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
        mistDiagnosticsVO.setEchoDeviceId("Test Echo Device Id");
        mistDiagnosticsVO.setMistlxChan((byte)1);
        mistDiagnosticsVO.setMistDeviceId("BB003003");
        mistDiagnosticsVO.setBaseStationHostname("Test Host");
        mistDiagnosticsVO.setBaseStationMac("Test MAC");
        mistDiagnosticsVO.setBaseStationPort((short) 120);
        mistDiagnosticsVO.setAreaName("Test Aeea");
        mistDiagnosticsVO.setAssetName("Test Asset");
        mistDiagnosticsVO.setSiteName("Test Site");
        mistDiagnosticsVO.setPointLocationName("Test PointLocation");
        mistDiagnosticsVO.setPointName("Test Point");
        mistDiagnosticsVO.setLastSample(LocalDateTime.parse("2022-11-08 02:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        mistDiagnosticsVO.setMistlxRssi((byte) 1);
        mistDiagnosticsVO.setMistlxFsamp((byte) 2);
        mistDiagnosticsVO.setMistlxLength((short) 1);
        mistDiagnosticsVO.setMistlxBattery(1f);
        mistDiagnosticsVO.setMistlxTemp(2f);
        mistDiagnosticsVO.setMistlxDelta((short) 2);
        mistDiagnosticsVO.setMistlxPcorr((short) 3);
        mistDiagnosticsVO.setMistlxDbg((byte) 3);
        mistDiagnosticsVO.setMistlxBleon((byte) 4);
        mistDiagnosticsVO.setMistlxFpgaon((byte) 5);
        mistDiagnosticsVO.setMistlxTotalMin((short) 4);
        mistDiagnosticsVO.setMistlxMincnt((short) 5);
        mistDiagnosticsVO.setMistlxFailcnt((short) 6);
        mistDiagnosticsVO.setMistlxCtime((short) 7);
        mistDiagnosticsVO.setMistlxCtimeMod((short) 8);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of CreateEchoDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test01_CreateEchoDiagnosticDeviceTelemetry() {
        System.out.println("CreateEchoDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New EchoDiagnostics created / updated successfully.\"}";
        String result = instance.createEchoDiagnostic(echoDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("CreateEchoDiagnostic createDeviceTelemetry Done");
    }
    
    /**
     * Test of CreateEchoDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test02_CreateEchoDiagnosticDeviceTelemetry() {
        System.out.println("CreateEchoDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New EchoDiagnostics created / updated successfully.\"}";
        String result = instance.createEchoDiagnostic(echoDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("CreateEchoDiagnostic createDeviceTelemetry Done");
    }

    
    /**
     * Test of CreateMistDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test03_CreateMistDiagnosticDeviceTelemetry() {
        System.out.println("CreateMistDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New MistDiagnostics created / updated successfully.\"}";
        String result = instance.createMistDiagnostic(mistDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("CreateMistDiagnostic createDeviceTelemetry Done");
    }
    
    /**
     * Test of CreateMistDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test04_CreateMistDiagnosticDeviceTelemetry() {
        System.out.println("CreateMistDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New MistDiagnostics created / updated successfully.\"}";
        String result = instance.createMistDiagnostic(mistDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("CreateMistDiagnostic createDeviceTelemetry Done");
    }
    
    /**
     * Test of CreateMistDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test05_CreateMistDiagnosticDeviceTelemetry() {
        System.out.println("CreateMistDiagnostic createDeviceTelemetry");
        DeviceTelemetryVO mistDVo = new DeviceTelemetryVO(mistDiagnosticsVO);
        mistDVo.setSiteId(null);
        String expResult = "{\"outcome\":\"Insufficient and/or invalid data for PK given in json\"}";
        String result = instance.createMistDiagnostic(mistDVo);
        assertEquals(expResult, result);
        System.out.println("CreateMistDiagnostic createDeviceTelemetry Done");
    }

    /**
     * Test of UpdateEchoDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test06_UpdateEchoDiagnosticDeviceTelemetry() {
        System.out.println("UpdateEchoDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New EchoDiagnostics created / updated successfully.\"}";
        String result = instance.createEchoDiagnostic(echoDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("UpdateEchoDiagnostic createDeviceTelemetry Done");
    }
    
    
    /**
     * Test of UpdateMistDiagnosticDeviceTelemetry method, of class DeviceTelemetryController.
     */
    @Test
    public void test07_UpdateMistDiagnosticDeviceTelemetry() {
        System.out.println("UpdateMistDiagnostic createDeviceTelemetry");
        String expResult = "{\"outcome\":\"New MistDiagnostics created / updated successfully.\"}";
        String result = instance.createMistDiagnostic(mistDiagnosticsVO);
        assertEquals(expResult, result);
        System.out.println("UpdateMistDiagnostic createDeviceTelemetry Done");
    }

}

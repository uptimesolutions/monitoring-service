/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.monitoring.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteBasestationDeviceControllerTest {
    
     @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    private static SiteBasestationDeviceController instance;
    public SiteBasestationDeviceControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SiteBasestationDeviceController();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of createSiteBasestationDevice method, of class SiteBasestationDeviceController.
     */
    @Test
    public void test0_CreateSiteBasestationDevice() {
        System.out.println("createSiteBasestationDevice");
        String content = "{\n" +
                    "  \"customer_acct\": \"FEDEX EXPRESS\",\n" +
                    "  \"site_id\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
                    "  \"base_station_port\":2023,\n" +
                    "  \"device_id\":\"BB002345\",\n" +
                    "  \"point_id\": \"9d217a12-8f2b-4317-877b-153687124d3b\",\n" +
                    "  \"fmax\": \"12\",\n" +
                    "  \"channel_type\": \"AC\",\n" +
                    "  \"resolution\": \"200\",\n" +
                    "  \"sample_interval\": \"20\",\n" +
                    "  \"sensor_channel_num\": \"1\",\n" +
                    "  \"last_sampled\":\"2023-04-10T16:22:00Z\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Created SiteDeviceBasestation successfully.\"}";
        String result = instance.createSiteBasestationDevice(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteBasestationDevice method, of class SiteBasestationDeviceController.
     * @throws java.lang.Exception
     */
    @Test
    public void test1_GetSiteBasestationDevice_pass() throws Exception {
        System.out.println("getSiteBasestationDevice");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        short baseStationPort = 2023;
        String expResult = "{\"customer_acct\":\"FEDEX EXPRESS\",\"site_id\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"base_station_port\":\"2023\",\"points\":[{\"device_id\":\"BB002345\",\"point_id\":\"9d217a12-8f2b-4317-877b-153687124d3b\",\"channel_type\":\"AC\",\"sensor_channel_num\":1,\"sample_interval\":20,\"fmax\":12,\"resolution\":200,\"is_disabled\":false,\"last_sampled\":\"2023-04-10T16:22:00Z\"}]}";
        String result = instance.getSiteBasestationDevice(customer, siteId, baseStationPort);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteBasestationDevice method, of class SiteBasestationDeviceController.
     * @throws java.lang.Exception
     */
    @Test
    public void test2_GetSiteBasestationDevice_fail() throws Exception {
        System.out.println("getSiteBasestationDevice");
        String customer = "88888";
        String siteId = "47bfb62a-bbe6-4345-8a86-08fdefb299e5";
        short baseStationPort = 1111;
        String expResult = "{\"outcome\":\"No items found for the given info.\"}";
        String result = instance.getSiteBasestationDevice(customer, siteId, baseStationPort);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteBasestationDevice method, of class SiteBasestationDeviceController.
     * customer_acct does not exist
     */
    @Test
    public void test3_UpdateSiteBasestationDevice_pass() {
        System.out.println("updateSiteBasestationDevice");
        String content = "{\n" +
                    "  \"customer_acct\": \"77777\",\n" +
                    "  \"site_id\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
                    "  \"base_station_port\":2023,\n" +
                    "  \"device_id\":\"BB002345\",\n" +
                    "  \"point_id\": \"9d217a12-8f2b-4317-877b-153687124d3b\",\n" +
                    "  \"last_sampled\":\"2023-04-10T01:20:00Z\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Updated SiteBasestationDevice successfully.\"}";
        String result = instance.updateSiteBasestationDevice(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSiteBasestationDevice method, of class SiteBasestationDeviceController.
     */
    @Test
    public void test4_UpdateSiteBasestationDevice_Pass() {
        System.out.println("updateSiteBasestationDevice");
        String content = "{\n" +
                    "  \"customer_acct\": \"FEDEX EXPRESS\",\n" +
                    "  \"site_id\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
                    "  \"base_station_port\":2023,\n" +
                    "  \"device_id\":\"BB002345\",\n" +
                    "  \"point_id\": \"9d217a12-8f2b-4317-877b-153687124d3b\",\n" +
                    "  \"last_sampled\":\"2023-04-10T01:20:00Z\"\n" +
                    "}";
        String expResult = "{\"outcome\":\"Updated SiteBasestationDevice successfully.\"}";
        String result = instance.updateSiteBasestationDevice(content);
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
}

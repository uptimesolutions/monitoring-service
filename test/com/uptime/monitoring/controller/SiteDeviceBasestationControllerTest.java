/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.monitoring.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteDeviceBasestationControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//monitor.cql", true, true, "worldview_dev1"));
    
    private static SiteDeviceBasestationController instance;
    
    public SiteDeviceBasestationControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SiteDeviceBasestationController();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSiteDeviceBasestationByCustomerSite method, of class SiteDeviceBasestationController.
     */
    @Test
    public void testGetSiteDeviceBasestationByCustomerSite() throws Exception {
        System.out.println("getSiteDeviceBasestationByCustomerSite");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String expResult = "{\"customer_acct\":\"FEDEX EXPRESS\",\"site_id\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"points\":[{\"device_id\":\"00123123\",\"base_station_port\":787,\"is_disabled\":false}]}";
        String result = instance.getSiteDeviceBasestationByCustomerSite(customer, siteId);
        System.out.println("result = " + result);
        assertEquals(expResult, result);
    }
    
}
